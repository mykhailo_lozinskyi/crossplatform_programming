import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Metacharacters {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// '^' metacharacter - search from the beginning of string
		Pattern pattern1 = Pattern.compile("^[a-e]");
		Matcher matcher1_1 = pattern1.matcher("a b c d 1 2 3 4"); // true - starts from 'a'
		System.out.println("1)\n" + matcher1_1.find());
		
		Matcher matcher1_2 = pattern1.matcher("fag h e");	// false - starts from 'f' ('f' !є [a-e])
		System.out.println(matcher1_2.find());
				
		// '$' metacharacter - search from the end of string
		Pattern pattern2 = Pattern.compile("[a-e]$");
		Matcher matcher2_1 = pattern2.matcher("a b c d 1 2 3 4");
		System.out.println("\n2)\n" + matcher2_1.find());
				
		Matcher matcher2_2 = pattern2.matcher("fag h e");
		System.out.println(matcher2_2.find());
				
		// '.' metacharacter - returns true if string contains at least one character
		Pattern pattern3 = Pattern.compile(".[0-9]");
		Matcher matcher3_1 = pattern3.matcher("a b c d");	// false
		System.out.println("\n3)\n" + matcher3_1.find());
		Matcher matcher3_2 = pattern3.matcher("123");		// true
		System.out.println(matcher3_2.find());
		Matcher matcher3_3 = pattern3.matcher("a b c 7 d");	// true ('7')
		System.out.println(matcher3_3.find());
				
		// '|' metacharacter - 'OR', alternative search
		Pattern pattern4 = Pattern.compile(".[0-9]|.[a-c]"); // search [0-9] or [a-c]
		Matcher matcher4_1 = pattern4.matcher("c b a d");	// true
		System.out.println("\n4)\n" + matcher4_1.find());
		Matcher matcher4_2 = pattern4.matcher("p 1 r e");	// true ('1')
		System.out.println(matcher4_2.find());
		Matcher matcher4_3 = pattern4.matcher("h k o l");	// false (no [0-9], no [a-c])
		System.out.println(matcher4_3.find());
				
		// '\\d' - search number character
		Pattern pattern5 = Pattern.compile("\\d");	// [0-9] is an alternative regex
		Matcher matcher5_1 = pattern5.matcher("a 1 g");	// true
		System.out.println("\n5)\n" + matcher5_1.find());
				
		Matcher matcher5_2 = pattern5.matcher("a c g");	// flase
		System.out.println(matcher5_2.find());
				
		// '\\D' - search NO number character
		Pattern pattern6 = Pattern.compile("\\D");	// [^0-9] is an alternative regex
		Matcher matcher6_1 = pattern6.matcher("111");	// false - only numbers
		System.out.println("\n6)\n" + matcher6_1.find());
						
		Matcher matcher6_2 = pattern6.matcher("a c g");	// true - No number
		System.out.println(matcher6_2.find());
		
		Matcher matcher6_3 = pattern6.matcher("fd 1 cg"); // true - no number characters available
		System.out.println(matcher6_3.find());
		
		// '\\s' - search for whitespace
		Pattern pattern7 = Pattern.compile("\\s"); // [ \\f\\n\\r\\t\\v] is an alternative regex
		Matcher matcher7_1 = pattern7.matcher("a 1	g");
		System.out.println("\n7)\n" + matcher7_1.find());
				
		Matcher matcher7_2 = pattern7.matcher("acg");
		System.out.println(matcher7_2.find());
		
		// '\\S' - search for NO whitespaces
		Pattern pattern8 = Pattern.compile("\\S"); // [ ^\\f\\n\\r\\t\\v] is an alternative regex
		Matcher matcher8_1 = pattern8.matcher("a 1	g"); // true - there are not only whitespaces ('a')
		System.out.println("\n8)\n" + matcher8_1.find());
				
		Matcher matcher8_2 = pattern8.matcher("acg");	// true - there are NO whitespaces at all
		System.out.println(matcher8_2.find());
		
		Matcher matcher8_3 = pattern8.matcher("\n \f");	// false - there are only whitespaces
		System.out.println(matcher8_3.find());
	}

}
