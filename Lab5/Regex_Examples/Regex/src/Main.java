import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		
		// Find all words at the beginning of a sentence where the length of the word is N letters
		Scanner scanner = new Scanner(System.in);
		int n = 0;  // Initialize n to a default value

        while (true) {
            try {
                System.out.print("Enter the desired word length (n): ");
                n = scanner.nextInt();
                if (n > 0) {
                    break;
                } else {
                    System.out.println("Please enter a positive integer.");
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("Invalid input. Please enter a positive integer.");
                scanner.nextLine();
            }
        }
		
		Pattern pattern1 = Pattern.compile("(?<=^|[.!?]\\s)\\w{" + n + "}\\b");
		String testString = "New world? Test string! Have a four letters. Next test string";
		Matcher matcher1 = pattern1.matcher(testString);
		
		while(matcher1.find()) {
			System.out.print("Match: "
				+ testString.substring(matcher1.start(), matcher1.end()));
			System.out.println(". Starts at index " + matcher1.start() 
				+ ", Ends at index " + matcher1.end() + ".");
		}
		
		scanner.close();
		
		System.out.println("\nThe program completed successfully.");
		
		/*
		// Find all negative whole numbers
		System.out.println("\nTask 2.");
		Pattern pattern2 = Pattern.compile("-\\d+");
		testString = "String with numbers: -5 8 12 -15 25 -18 50";
		Matcher matcher2 = pattern2.matcher(testString); 
		while(matcher2.find()) {
			System.out.print("Match: "
				+ testString.substring(matcher2.start(), matcher2.end()));
			System.out.println(". Starts at index " + matcher2.start() 
				+ ", Ends at index " + matcher2.end() + ".");
		}
		
		// Find all negative fractional numbers
		System.out.println("\nTask 3.");
		Pattern pattern3 = Pattern.compile("-\\d+\\.\\d+");
		String testString = "String with numbers: -0.25 8 12 -15.0 25 -18.5245 50";
		Matcher matcher3 = pattern3.matcher(testString); 
		while(matcher3.find()) {
			System.out.print("Match: "
				+ testString.substring(matcher3.start(), matcher3.end()));
			System.out.println(". Starts at index " + matcher3.start() 
				+ ", Ends at index " + matcher3.end() + ".");
		}*/
		
		// Grouping
		/*Pattern groupingPattern1 = Pattern.compile("(\\d+)");
		Matcher matcher1_1 = groupingPattern1.matcher("2016 year, 2017 year, 2018 year");
		
		System.out.println("1)");
		while(matcher1_1.find()) {
			System.out.println(matcher1_1.group(1));	// 2016 2017 2018
		}
		
		
		Pattern groupingPattern2 = Pattern.compile("(\\d+).*\\1"); // repeating number groups
		Matcher matcher2_1 = groupingPattern2.matcher("2016 year, 2017 year, 2018 year");
		
		System.out.println("\n2)");
		while(matcher2_1.find()) {
			System.out.println(matcher2_1.group(1));	// 201
		}
		
		// If the group is used only for grouping and we are not interested in the result, then before the template - '?:'
		// In this case, the match is not remembered
		// (?:Mouse|Keyboard)Listener - this regex is looking for any string that has "MouseListener" or "KeyboardListener" as its suffix.
		Pattern groupingPattern3 = Pattern.compile("(?:Mouse|Keyboard)Listener"); // repeating number groups
		Matcher matcher3_1 = groupingPattern3.matcher("MouseListener, Keyboard Listender");
		
		System.out.println("\n3)");
		while(matcher3_1.find()) {
			System.out.println(matcher3_1.group());	// MouseListener
		}
		
		// Positive lookahead
		Pattern groupingPattern4 = Pattern.compile("Java (?=7|8)");
		Matcher matcher4_1 = groupingPattern4.matcher("Java 7, Java 8");
		System.out.println("\n4)");
		while(matcher4_1.find()) {
			System.out.println(matcher4_1.group());	// Java Java
		}
		
		// Negative lookahead
		Pattern groupingPattern5 = Pattern.compile("(Java|Python) (?!7|8)");
		Matcher matcher5_1 = groupingPattern5.matcher("Python 1, Java 7, Java 9");
		System.out.println("\n5)");
		while(matcher5_1.find()) {
			System.out.println(matcher5_1.group());	// Python Java
		}
		
		// Positive lookbehind
		Pattern groupingPattern6 = Pattern.compile("(?<=\\d\\))Java");
		Matcher matcher6_1 = groupingPattern6.matcher("1)Java; 2)Kotlin; 3)Java");
		System.out.println("\n6)");
		while(matcher6_1.find()) {
			System.out.println(matcher6_1.group());	// Java Java
		}
		
		// Negative lookbehind
		Pattern pattern7 = Pattern.compile("(?<!\\D\\))(Java|Python)");
		Matcher matcher7_1 = pattern7.matcher("1)Python; 2)R; 3)Java");
		System.out.println("\n7)");
		while(matcher7_1.find()) {
			System.out.println(matcher7_1.group());	// Python Java
		}*/
	}
	
}
