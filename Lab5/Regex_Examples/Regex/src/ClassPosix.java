import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class ClassPosix {
	public static void main(String[] args) {
		int counter = 0;
		String string = "abcd1234";
		
		Pattern pattern = Pattern.compile("\\p{Digit}");
		Matcher matcher = pattern.matcher(string);
		
		while(matcher.find()) {
			counter += 1;
			System.out.print("Match: '" + string.substring(matcher.start(), matcher.end()) 
			+ "'. ");
			System.out.println("Pattern found from " + matcher.start() +
                    " to " + (matcher.end()) + ".");
		}
		System.out.println("Matches found: " + counter);
	}
}
