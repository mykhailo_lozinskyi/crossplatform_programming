package quantifiers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ReluctantQuantifier {
	public static void main(String[] args) {
		
		// Making an instance of Pattern class
        // Here "+" is a Reluctant quantifier because a '?' is appended after it.
		String string1_1 = "ggg";
        Pattern p1 = Pattern.compile("g+?");
        
        System.out.println("Pattern: 'g+?'\nString: '" + string1_1 + "'");
        Matcher m1_1 = p1.matcher(string1_1);
        while (m1_1.find()) {
        	System.out.println("\nPattern found from " + m1_1.start() +
                    " to " + (m1_1.end()-1));
        	System.out.println("Match: '" + string1_1.substring(m1_1.start(), m1_1.end()) 
        						+ "'");
        }
        
        System.out.println("\n/************************************************/");
        String string1_2 = "gafggh2g";
        System.out.println("Pattern: 'g+?'\nString: '" + string1_2 + "'");
        Matcher m1_2 = p1.matcher(string1_2);
        while (m1_2.find()) {
        	System.out.println("\nPattern found from " + m1_2.start() +
                    " to " + (m1_2.end()-1));
        	System.out.println("Match: '" + string1_2.substring(m1_2.start(), m1_2.end()) 
			+ "'");
        }
        
        System.out.println("\n/************************************************/");
        // Example 2
        // Find all matches in which the number 19 is either not repeated or repeated many times
        int counter = 0;
        String string2 = "196.198.1.197";
        Pattern p2 = Pattern.compile(".*?19");
        Matcher m2_1 = p2.matcher(string2);
        while(m2_1.find()) {
        	System.out.println("Pattern found from " + m2_1.start() +
                    " to " + (m2_1.end()));
        	System.out.println("Match: '" + string2.substring(m2_1.start(), m2_1.end()) 
			+ "'");
        	counter += 1;
        }
        
        System.out.println("Matches found: " + counter);
	}
}
