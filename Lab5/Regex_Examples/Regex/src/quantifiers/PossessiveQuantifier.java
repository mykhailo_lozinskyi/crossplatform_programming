package quantifiers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PossessiveQuantifier {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Making an instance of Pattern class
        // Here "+" is a Possessive quantifier because a "+' is appended after it.
		String string1_1 = "ggg";
        Pattern p1 = Pattern.compile("g++");
        
        System.out.println("Pattern: 'g++'\nString: '" + string1_1 + "'");
        Matcher m1_1 = p1.matcher(string1_1);
        while (m1_1.find()) {
        	System.out.println("\nPattern found from " + m1_1.start() +
                    " to " + (m1_1.end()-1));
        	System.out.println("Match: '" + string1_1.substring(m1_1.start(), m1_1.end()) 
        						+ "'");
        }
        
        String string1_2 = "gafggh2g";
        System.out.println("\n/************************************************/");
        System.out.println("Pattern: 'g++'\nString: '" + string1_2 + "'");
        Matcher m1_2 = p1.matcher(string1_2);
        while (m1_2.find()) {
        	System.out.println("\nPattern found from " + m1_2.start() +
                    " to " + (m1_2.end()-1));
        	System.out.println("Match: '" + string1_2.substring(m1_2.start(), m1_2.end()) 
			+ "'");
        }
        
        
        System.out.println("\n/************************************************/");
        String string1_3 = "ggg";
        // Possessive vs Greedy Quantifiers
        // Create a pattern with Greedy quantifier
        Pattern pg = Pattern.compile("g+g");
 
        // Create same pattern with possessive quantifier
        Pattern pp = Pattern.compile("g++g");         
 
        System.out.println("Using Greedy Quantifier");
        System.out.println("Pattern: 'g+g'\nString: '" + string1_3 + "'");
        Matcher mg = pg.matcher(string1_3); 
        while (mg.find())
        {
        	System.out.println("Pattern found from " + mg.start() +
                    " to " + (mg.end()-1)); 
        }
            
        System.out.println("\nUsing Possessive Quantifier");
        System.out.println("Pattern: 'g++g'\nString: '" + string1_3 + "'");
        Matcher mp = pp.matcher(string1_3); 
        while (mp.find())
        {
        	System.out.println("Pattern found from " + mp.start() +
                    " to " + (mp.end()-1)); 
        } 
        
        System.out.println("\n/************************************************/");
        // Example 2
        int counter = 0;
        String string = "192.168.1.197";
        Pattern pattern = Pattern.compile(".*+19");
        Matcher matcher = pattern.matcher(string);
        while(matcher.find()) {
        	System.out.println("Pattern found from " + matcher.start() +
                    " to " + (matcher.end()));
        	System.out.println("Match: '" + string.substring(matcher.start(), matcher.end()) 
			+ "'");
        	counter += 1;
        }
        
        System.out.println("Matches found: " + counter);
	}

}
