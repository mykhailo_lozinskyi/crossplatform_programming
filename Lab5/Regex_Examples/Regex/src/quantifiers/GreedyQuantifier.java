package quantifiers;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class GreedyQuantifier {

	public static void main(String[] args) {
		
		// true - if '1' is followed by two or more '0's, returns 100
		System.out.println("10{2} and 100 = " 
					+ Pattern.compile("10{2}").matcher("100").find());
		System.out.println("10{2} and 1000 = " 
					+ Pattern.compile("10{2}").matcher("1000").find());
		
		// true - if '1' is followed by one-three or more '0's, returns 10, 100 or 1000
		System.out.println("\n10{1,3} and 10 = " 
					+ Pattern.compile("10{1,3}").matcher("10").find()); // true
		System.out.println("10{1,3} and 100 = " 
					+ Pattern.compile("10{1,3}").matcher("100").find());// true
		System.out.println("10{1,3} and 1000 = " 
				+ Pattern.compile("10{1,3}").matcher("1000").find());	 // true
		System.out.println("10{1,3} and 1010 = " 
				+ Pattern.compile("10{1,3}").matcher("1010").find());	 // true ('10')
		System.out.println("10{1,3} and 1 = " 
				+ Pattern.compile("10{1,3}").matcher("1").find());	 	 // false
		
		// true - if '1' is followed by two or more '0's, returns 100..0
		System.out.println("\n10{2,} and 1000 = " 
				+ Pattern.compile("10{2,}").matcher("1000").find()); 	 // true
		
		// '0?' - a single 0 may or may not be present
		System.out.println("\n10? and 1 = " 
				+ Pattern.compile("10?").matcher("1").find()); 	 // true
		System.out.println("10? and 10 = " 
				+ Pattern.compile("10?").matcher("10").find());  // true
		System.out.println("10? and 11 = " 
				+ Pattern.compile("10?").matcher("11").find());  // true
		System.out.println("10? and 100 = " 
				+ Pattern.compile("10?").matcher("100").find()); // true
		
		System.out.println("\n/************************************************/");
		// Example 2
		int counter = 0;
		String string = "196.198.1.197";
		Pattern pattern = Pattern.compile(".*19");
		Matcher matcher = pattern.matcher(string);
		
		while(matcher.find()) {
			System.out.println("Pattern found from " + matcher.start() +
                    " to " + (matcher.end()));
        	System.out.println("Match: '" + string.substring(matcher.start(), matcher.end()) 
			+ "'");
        	counter += 1;
		}
		
		System.out.println("Matches found: " + counter);
	}

}
