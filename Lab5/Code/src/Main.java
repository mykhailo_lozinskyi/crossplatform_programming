import text.TextManager;

public class Main {
	
	public static void main(String[] args) {
		TextManager txtManager = new TextManager();
		System.out.println("Enter the text (terminate with an empty line):");
		txtManager.ReadText();
		
		var sentencesPenaltimateWords = txtManager.findPenultimateWords();
		if(sentencesPenaltimateWords != null) {
			if(!sentencesPenaltimateWords.isEmpty()) {
				String penaltimateWord;
				StringBuilder resultSB = new StringBuilder();
				for(var sentencePenaltimateWord: sentencesPenaltimateWords) {
					resultSB.append("Sentence:" + sentencePenaltimateWord.getKey() + "\n");
					penaltimateWord = sentencePenaltimateWord.getValue();
					
					if(!penaltimateWord.isEmpty()) {
						resultSB.append("Penaltimate Word: " + penaltimateWord);
					} else {
						resultSB.append("This sentence does not have a penultimate word because it "
								+ "contains only one word.");
					}
					resultSB.append("\n");
					System.out.println(resultSB.toString());
					
					resultSB.setLength(0);
				}
			} else {
				System.out.println("The text does not contain punctuation marks, "
						+ "so it was not possible to highlight any of the penultimate "
						+ "words.");
			}
		} else {
			System.out.println("The text is empty. That is, the text "
					+ "does not contain any sentence.");
		}
		
		System.out.println("\nThe program completed its execution successfully!");
	}
}
