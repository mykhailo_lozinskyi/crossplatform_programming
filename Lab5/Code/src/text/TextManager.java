package text;

import java.io.InputStream;
import java.util.Scanner;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

public class TextManager {
	private String text;
	
	public TextManager() {
		this.text = "";
	}
	
	public TextManager(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void ReadText(InputStream inputStream) {
		 Scanner scanner = new Scanner(inputStream);
		 
		 StringBuilder inputText = new StringBuilder();
		 
		 String currentLine;
		 while(true) {
			 currentLine = scanner.nextLine();
			 if(currentLine.isEmpty()) {
				 break;
			 }
			 inputText.append(currentLine);
			 if(!currentLine.endsWith(" ")) {
				 inputText.append(" ");
			 }
		 }
		 
		 scanner.close();
		 this.text = inputText.toString().trim();
	}
	
	public void ReadText() {
		ReadText(System.in);
	}
	
	public List<String> splitIntoSentences() {
		Pattern sentencePattern = Pattern.compile("[^.!?]+[.!?]");
		Matcher sentenceMatcher = sentencePattern.matcher(text);
		
		ArrayList<String> sentencesList = new ArrayList<String>();
		
		while(sentenceMatcher.find()) {
			sentencesList.add(text.substring(sentenceMatcher.start(), sentenceMatcher.end()));
		}
		
		return sentencesList;
	}
	
	public String findPenultimateWord(String sentence) {
		Pattern penultimateWordPattern = Pattern.compile("\\b\\w+(-\\w+)?(?=([,;:]|(\\s-))?\\s+\\w+(-\\w+)?[.!?])");
		Matcher penultimateWordMatcher = penultimateWordPattern.matcher(sentence);
		
		if(penultimateWordMatcher.find()) {
			return sentence.substring(penultimateWordMatcher.start(), penultimateWordMatcher.end());
		}
		return null;
	}
	
	public List<Map.Entry<String, String>> findPenultimateWords(){
		if(this.text.isEmpty()) {
			return null;
		}
		List<Map.Entry<String, String>> sentencePenultimateWordList = new ArrayList<>();
		var sentences = splitIntoSentences();
		
		String penultimateWord;
		for(var sentence: sentences) {
			penultimateWord = findPenultimateWord(sentence);
			sentencePenultimateWordList.add(Map.entry(sentence, penultimateWord != null ? penultimateWord: ""));
		}
		
		return sentencePenultimateWordList;
	}
	

}
