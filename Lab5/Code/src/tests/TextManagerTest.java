package tests;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import text.TextManager;

public class TextManagerTest {
	
	@Test
	public void splitIntoSentencesTest() {
		String testText = "Hello World from Java and Eclipse!"
				+ "Java is a widely-used programming language for coding "
				+ "web applications."
				+ "Java is a multi-platform, object-oriented, and network-centric "
				+ "language.";
		
		ArrayList<String> expectedRes = new ArrayList<String>(List.of(
				"Hello World from Java and Eclipse!",
				"Java is a widely-used programming language for coding web "
				+ "applications.",
				"Java is a multi-platform, object-oriented, and network-centric "
				+ "language.")
				);
		
		TextManager txtManager = new TextManager(testText);
		var actualRes = txtManager.splitIntoSentences();
		
		assertTrue(actualRes instanceof ArrayList);
	    assertEquals(expectedRes.size(), actualRes.size());
	    assertIterableEquals(expectedRes, actualRes);
	}
	
	@Test
	public void findPenultimateWordTest() {
		String testSentence = "JUnit is a unit testing framework for Java "
				+ "programming language.";
		String expectedRes = "programming";
		
		TextManager txtManager = new TextManager();
		var actualRes = txtManager.findPenultimateWord(testSentence);
		
		assertEquals(expectedRes, actualRes);
	}
	
	@Test
	public void findPenultimateWordsOriginCaseTest() {
		String testText = "Hello World from Java and Eclipse!"
				+ "Java is a widely-used programming language for coding "
				+ "web applications."
				+ "Java is a multi-platform, object-oriented, and network-centric "
				+ "language.";
		List<Map.Entry<String, String>> expectedRes = 
				new ArrayList<Map.Entry<String, String>>();
		expectedRes.add(Map.entry("Hello World from Java and Eclipse!", "and"));
		expectedRes.add(Map.entry("Java is a widely-used programming language for "
				+ "coding web applications.", "web"));
		expectedRes.add(Map.entry("Java is a multi-platform, object-oriented, and "
				+ "network-centric language.", "network-centric"));
		
		TextManager txtManager = new TextManager(testText);
		var actualRes = txtManager.findPenultimateWords();
		
		assertTrue(actualRes instanceof ArrayList<Map.Entry<String, String>>);
		assertEquals(expectedRes.size(), actualRes.size());
		assertIterableEquals(expectedRes, actualRes);
	}
	
	@Test
	public void findPenultimateWordsEmptyTextTest() {
		String testText = "";
		List<Map.Entry<String, String>> expectedRes = null;
		
		TextManager txtManager = new TextManager(testText);
		var actualRes = txtManager.findPenultimateWords();
		
		assertEquals(expectedRes, actualRes);
	}
	
	@Test
	public void findPenultimateWordsNoLexemesTest() {
		String testText = "Text without lexemes"
				+ " This means that the given text will not have punctuation marks that mark "
				+ "the end of the sentence";
		
		TextManager txtManager = new TextManager(testText);
		var actualRes = txtManager.findPenultimateWords();
		
		assertTrue(actualRes.isEmpty());
	}
	
	@Test
	public void findPenultimateWordsOneWordSentencesTest() {
		String testText = "Sky. What color is it? Blue? Cyan?"
				+ " A cloudless sky in the daytime, appears blue to the human eye.";
		List<Map.Entry<String, String>> expectedRes = 
				new ArrayList<Map.Entry<String, String>>();
		expectedRes.add(Map.entry("Sky.", ""));
		expectedRes.add(Map.entry(" What color is it?", "is"));
		expectedRes.add(Map.entry(" Blue?", ""));
		expectedRes.add(Map.entry(" Cyan?", ""));
		expectedRes.add(Map.entry(" A cloudless sky in the daytime, appears blue to "
				+ "the human eye.", "human"));
		
		TextManager txtManager = new TextManager(testText);
		var actualRes = txtManager.findPenultimateWords();
		
		assertTrue(actualRes instanceof ArrayList<Map.Entry<String, String>>);
		assertEquals(expectedRes.size(), actualRes.size());
		assertIterableEquals(expectedRes, actualRes);
	}
	
	@Test
	public void findPenultimateWordsTwoWordSentencesTest() {
		String testText = "Blue sky. Yellow sun. Green grass."
				+ " A wonderful summer day!";
		List<Map.Entry<String, String>> expectedRes = 
				new ArrayList<Map.Entry<String, String>>();
		expectedRes.add(Map.entry("Blue sky.", "Blue"));
		expectedRes.add(Map.entry(" Yellow sun.", "Yellow"));
		expectedRes.add(Map.entry(" Green grass.", "Green"));
		expectedRes.add(Map.entry(" A wonderful summer day!", "summer"));
		
		TextManager txtManager = new TextManager(testText);
		var actualRes = txtManager.findPenultimateWords();
		
		assertTrue(actualRes instanceof ArrayList<Map.Entry<String, String>>);
		assertEquals(expectedRes.size(), actualRes.size());
		assertIterableEquals(expectedRes, actualRes);
	}
	
	@Test
	public void findPenultimateWordsPunctuationTest() {
		String testText = "Hello, World!"
				+ "Hello from Java."
				+ "Red: Cyan."
				+ "Green - Magenta."
				+ "Blue; Yellow."
				+ "RGB: R - Red, G - Green, B - Blue.";
		List<Map.Entry<String, String>> expectedRes = 
				new ArrayList<Map.Entry<String, String>>();
		expectedRes.add(Map.entry("Hello, World!", "Hello"));
		expectedRes.add(Map.entry("Hello from Java.", "from"));
		expectedRes.add(Map.entry("Red: Cyan.", "Red"));
		expectedRes.add(Map.entry("Green - Magenta.", "Green"));
		expectedRes.add(Map.entry("Blue; Yellow.", "Blue"));
		expectedRes.add(Map.entry("RGB: R - Red, G - Green, B - Blue.", "B"));
		
		TextManager txtManager = new TextManager(testText);
		var actualRes = txtManager.findPenultimateWords();
		
		assertTrue(actualRes instanceof ArrayList<Map.Entry<String, String>>);
		assertEquals(expectedRes.size(), actualRes.size());
		assertIterableEquals(expectedRes, actualRes);
	}
}
