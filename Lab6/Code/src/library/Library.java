package library;

import helpers.MailHelper;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Optional;
import java.util.Comparator;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import java.util.stream.Collectors;

import java.io.Serializable;

public class Library implements Serializable {
	private static final long serialVersionUID = 2L;
	private List<Book> books;
    private List<Subscription> subscriptions;
    private List<BookRecord> bookRecords;
    private Administrator administrator;
    private MailHelper mailHelper;
    
    public Library() {
		this.mailHelper = new MailHelper(System.getenv("JavaLab6SenderEmail"), System.getenv("JavaLab6SenderPassword"));
    }
    
    public Library(List<Book> books) {
    	this();
    	this.books = books;
    }
    
    public Library(List<Book> books, Administrator administrator) {
    	this();
    	this.books = books;
    	this.administrator = administrator;
    }
    
    public Library(List<Book> books, List<Subscription> subscriptions, Administrator administrator) {
    	this();
        this.books = books;
        this.subscriptions = subscriptions;
        this.bookRecords = new ArrayList<>();
        this.administrator = administrator;
    }

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
	
	public void addBook(Book book) {
		if(book != null) {
			this.books.add(book);
		}
	}
	
	public void removeBook(Book book) {
		this.books.removeIf(currBook -> currBook.equals(book));
	}
	
	public List<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}
	
	public void addSubscription(Subscription subscription) {
		if(subscription != null) {
			this.subscriptions.add(subscription);
		}
	}
	
	public void removeSubscription(Subscription subscription) {
		this.subscriptions.removeIf(currSubscription -> currSubscription.equals(subscription));
	}
	
	public List<BookRecord> getBookRecords() {
		return bookRecords;
	}

	public void setBookRecords(List<BookRecord> bookRecords) {
		this.bookRecords = bookRecords;
	}
	
	public void addBookRecord(BookRecord bookRecord) {
		if(bookRecord != null) {
			this.bookRecords.add(bookRecord);
		}
	}
	
	public void removeBookRecord(BookRecord bookRecord) {
		this.bookRecords.removeIf(currBook -> currBook.equals(bookRecord));
	}
	
	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}
	
	public MailHelper getMailHelper() {
		return mailHelper;
	}

	public Book getBookByBarcode(String barcode) {
	    return books.stream()
	            .filter(book -> book.getBarcode().equals(barcode))
	            .findFirst()
	            .orElse(null);
	}
	
	public boolean isBookTaken(Book book) {
        for (BookRecord record : this.getBookRecords()) {
            if (record.getBook().equals(book) && record.getActualReturnDate() == null) {
                // The book is already taken and not returned
                return true;
            }
        }
        return false;
    }
	
	public List<Book> getNovelties(int numberOfNovelties){
		if(numberOfNovelties <= 0) {
			throw new IllegalArgumentException("Number of novelties must be greater than zero");
		}
		var booksList = books.stream()
                .collect(Collectors.toMap(
                        book -> List.of(book.getAuthor(), book.getTitle(), book.getYearOfPublication()),
                        book -> book,
                        (existing, replacement) -> existing,
                        LinkedHashMap::new
                ))
                .values()
                .stream()
                .collect(Collectors.toList());
		if(numberOfNovelties >= booksList.size()) {
			return booksList;
		}
		int noveltyIndex = booksList.size() - numberOfNovelties;
		var result = booksList.subList(noveltyIndex, booksList.size());
		Collections.reverse(result);
		return result;
	}
	
	public List<Book> getNovelties(){
		return this.getNovelties(2);
	}
	
	public List<BookRecord> getBooksToReturn(Subscription subscription) {
		return this.getBookRecords().stream()
				.filter(bookRecord -> (subscription.getTakenBooks().contains(bookRecord.getBook())
										&& bookRecord.getActualReturnDate() == null))
				.collect(Collectors.toList());
	}
	
	// Task 1
	public List<Book> getSortedBooksByYearOfPublication(boolean reverse){
		Comparator<Book> comparator = Comparator.comparingInt(Book::getYearOfPublication);
	    
	    if (reverse) {
	        comparator = comparator.reversed();
	    }
	    
		return this.getBooks().stream()
				.sorted(comparator)
				.collect(Collectors.toList());
	}
	
	public List<Book> getSortedBooksByYearOfPublication(){
		return this.getSortedBooksByYearOfPublication(false);
	}
	
	// Task 2
	public List<String> getReaderEmails(int numberOfBooks, boolean outputIntermediateResults){
		var subscriptionsList = 
				this.getSubscriptions().stream()
									   .filter((subscription) -> (subscription.getTakenBooks().size() > numberOfBooks))
									   .collect(Collectors.toList());
		if(outputIntermediateResults) {
			int longestEmailLength = 25;
			Optional<String> longestEmailOptional = subscriptionsList.stream()
																	 .map(Subscription::getEmail)
			        										  		 .max(Comparator.comparingInt(String::length));
			if(longestEmailOptional.isPresent()) {
				longestEmailLength = longestEmailOptional.get().length();
			}
			System.out.println(String.format("%" + longestEmailLength + "s\tNumber of taken books", "Email"));
			String intermediateResultsStringFormat = "%" + longestEmailLength + "s\t%3d";
			subscriptionsList.stream()
							 .forEach(subscription -> 
							 	System.out.println(String.format(intermediateResultsStringFormat, 
										subscription.getEmail(), subscription.getTakenBooks().size())));
		}
		return subscriptionsList.stream()
								.map(Subscription::getEmail)
								.collect(Collectors.toList());
	}
	
	public List<String> getReaderEmails(int numberOfBooks){
		return this.getReaderEmails(numberOfBooks, true);
	}
	
	// Task 3
	public long getNumberOfTakenAuthorBooks(String author) {
		return this.getSubscriptions().stream()
				.filter((subscription) -> (subscription.getTakenBooks().stream()
						.anyMatch(book -> book.getAuthor().equals(author))))
				.count();
	}
	
	// Task 4
	public int getLargestNumberOfTakenBooks() {
		int maxBooks = 0;
		Optional<Integer> maxBooksTaken = 
				this.getSubscriptions().stream()
									   .map(subscription -> subscription.getTakenBooks().size())
									   .max(Comparator.naturalOrder());
		
		if (maxBooksTaken.isPresent()) {
			maxBooks = maxBooksTaken.get();
		}
		
		return maxBooks;
	}
	
	public List<Subscription> getReadersWithLargestNumberOfTakenBooks(){
		int maxBooks = this.getLargestNumberOfTakenBooks();
		if(maxBooks == 0) {
			return new ArrayList<Subscription>();
		}
		return this.getSubscriptions().stream()
									  .filter(subscription -> subscription.getTakenBooks().size() == maxBooks)
									  .collect(Collectors.toList());
	}
	
	// Task 5
	public Map<Integer, List<Subscription>> getUserGroups(int numberOfBooks) {
        Map<Integer, List<Subscription>> userGroups = this.getSubscriptions().stream()
                .collect(Collectors.groupingBy(subscription -> {
                    int takenBooksCount = subscription.getTakenBooks().size();
                    return takenBooksCount < numberOfBooks ? 1 : 2;
                }));

        return userGroups;
    }
	
	// Task 6
	public Map<Subscription, Map<Book, Long>> findDebtors(LocalDate currentDate) {
	    Map<Subscription, Map<Book, Long>> debtorsMap = subscriptions.stream()
	            .collect(Collectors.toMap(
	                    subscription -> subscription,
	                    subscription -> {
	                        List<BookRecord> overdueBooks = getOverdueBooks(subscription, currentDate);
	                        if (!overdueBooks.isEmpty()) {
	                            Map<Book, Long> overdueBooksMap = new LinkedHashMap<>();
	                            overdueBooks.forEach(bookRecord -> {
	                                long daysOverdue = calculateDaysOverdue(bookRecord.getPlannedReturnDate(), currentDate);
	                                overdueBooksMap.put(bookRecord.getBook(), daysOverdue);
	                            });
	                            return overdueBooksMap;
	                        } else {
	                            return new LinkedHashMap<Book, Long>();
	                        }
	                    }
	            ))
	            .entrySet()
	            .stream()
	            .filter(entry -> entry.getValue() != null)
	            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

	    return debtorsMap;
	}
	
	private List<BookRecord> getOverdueBooks(Subscription subscription, LocalDate currentDate){
		return this.getBookRecords().stream()
				.filter(bookRecord -> (subscription.getTakenBooks().contains(bookRecord.getBook())
										&& bookRecord.getPlannedReturnDate().isBefore(currentDate)
										&& bookRecord.getActualReturnDate() == null))
				.collect(Collectors.toList());
	}
	
	public List<String> getDebtorsInfo(LocalDate currentDate){
		var debtorsMap = this.findDebtors(currentDate);
		StringBuilder debtorInfoSb = new StringBuilder();
		Subscription currSubscription = null;
		Book currBook = null;
		List<String> debtorsInfoList = new ArrayList<String>();
		for(var debtorEntry: debtorsMap.entrySet()) {
			currSubscription = debtorEntry.getKey();
			var debtorBooksInfo = debtorEntry.getValue();
			if(debtorBooksInfo.isEmpty()) {
				continue;
			}
			debtorInfoSb.append("Debtor: ").append(currSubscription.getFirstName() + " " 
					+ currSubscription.getLastName());
			debtorInfoSb.append("; Overdue Books: ");
			
			for(var debtorBookEntry: debtorBooksInfo.entrySet()) {
				currBook = debtorBookEntry.getKey();
				debtorInfoSb.append("(" + currBook.getBarcode() + "; \""
      				  + currBook.getTitle() + "\"; ");
				debtorInfoSb.append("Days overdue: " + debtorBookEntry.getValue() + "); ");
			}
			debtorsInfoList.add(debtorInfoSb.toString());
			debtorInfoSb.setLength(0);
		}
		return debtorsInfoList;
	}
	
	public long calculateDaysOverdue(LocalDate plannedReturnDate, LocalDate currentDate) {
        return Math.max(0, ChronoUnit.DAYS.between(plannedReturnDate, currentDate));
    }
}
