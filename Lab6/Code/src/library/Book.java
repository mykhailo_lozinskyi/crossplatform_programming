package library;

import java.util.Objects;
import java.io.Serializable;

public class Book implements Serializable {
	private static final long serialVersionUID = 1L;
	private String barcode;
	private String author;
    private String title;
    private int yearOfPublication;
    
	public Book(String barcode, String author, String title, int yearOfPublication) {
		this.setBarcode(barcode);
		this.setAuthor(author);
		this.setTitle(title);
		this.setYearOfPublication(yearOfPublication);
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYearOfPublication() {
		return yearOfPublication;
	}

	public void setYearOfPublication(int yearOfPublication) {
		this.yearOfPublication = yearOfPublication;
	}
	
	private void setBarcode(String barcode) {
		String barcodeRegex = "\\d{12}";
		if (barcode.matches(barcodeRegex)) {
            this.barcode = barcode;
        } else {
            throw new IllegalArgumentException("Invalid barcode format");
        }
	}
	
	public String getBarcode() {
		return barcode;
	}
	
	@Override
	public String toString() {
		return "Book{"
				+ "barcode=" + barcode
				+ ",author=" + author
				+ ",title=\"" + title + "\""
				+ ",yearOfPublication=" + yearOfPublication
				+ "}";
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this)
		{
			return true;
		}
		if (!(o instanceof Book))
		{
			return false; 
		}
		Book book = (Book) o;
        return barcode.equals(book.getBarcode())
        		&& author.equals(book.getAuthor())
        		&& title.equals(book.getTitle())
        		&& yearOfPublication == book.getYearOfPublication();       
	}
	
	@Override
	public int hashCode() {
        return Objects.hash(barcode);
    }
}
