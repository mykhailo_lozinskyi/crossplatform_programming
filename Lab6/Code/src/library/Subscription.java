package library;

import java.util.List;
import java.util.Objects;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.Serializable;

public class Subscription implements Serializable {
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastName;
    private String middleName;
    private String email;
    private List<Book> takenBooks;
    
	public Subscription(String firstName, String lastName, String middleName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.setEmail(email);
		this.takenBooks = new ArrayList<>();
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (isValidEmail(email)) {
            this.email = email;
        } else {
            throw new IllegalArgumentException("Invalid email address");
        }
	}
	
	private boolean isValidEmail(String email) {
        String emailRegex = 
        		"^(\\w+(\\.[\\w-]+)*@[\\w-]+((\\.[\\d\\p{Alpha}]+)*(\\.\\p{Alpha}{2,})*)*)$";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

	public List<Book> getTakenBooks() {
		return takenBooks;
	}

	public void setTakenBooks(List<Book> takenBooks) {
		this.takenBooks = takenBooks;
	}
	
	public void addTakenBook(Book book) {
		this.takenBooks.add(book);
	}
	
	public void removeReturnedBook(Book book) {
		if(!this.takenBooks.contains(book)) {
			throw new IllegalArgumentException("Book not found or already returned");
		}
		this.takenBooks.remove(book);
	}
	
	public String getDescription(boolean detailed) {
		StringBuilder subscriptionSb = new StringBuilder();
		subscriptionSb.append("Full Name=" + this.firstName + " " + 
							   this.lastName + " " + this.middleName);
		subscriptionSb.append("; Email=\"" + this.email + "\"");
		if((takenBooks != null) && (!takenBooks.isEmpty())) {
			subscriptionSb.append("; Taken Books: ");
			for(var book: takenBooks) {
				if(detailed) {
					subscriptionSb.append(book.toString() + "; ");
				} else {
					subscriptionSb.append("Book=" + book.getBarcode() + "; ");
				}
			}
		}
		return subscriptionSb.toString();	
	}
	
	public String getDescription() {
		return this.getDescription(false);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this)
		{
			return true;
		}
		if (!(o instanceof Subscription))
		{
			return false; 
		}
		Subscription subscription = (Subscription) o;
        return firstName.equals(subscription.getFirstName())
        		&& lastName.equals(subscription.getLastName())
        		&& middleName.equals(subscription.getMiddleName())
        		&& email.equals(subscription.getEmail())
        		&& Objects.equals(takenBooks, subscription.getTakenBooks());       
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(firstName, lastName, middleName, email, takenBooks);
    }
}
