package library;

import java.time.LocalDate;
import java.io.Serializable;

public class Administrator implements Serializable {
	private static final long serialVersionUID = 1L;
	private String username;
	
	public Administrator(String username) {
        this.username = username;
    }
	
	public String getUsername() {
		return this.username;
	}
	
	public void recordBookTaking(Library library, 
			Subscription subscription, 
			Book book, 
			LocalDate takeDate, 
			LocalDate plannedReturnDate) {
		if (library.getAdministrator() != this) {
            throw new IllegalArgumentException("This administrator is not associated with "
            		+ "the provided library.");
        }
		
		// Check if the book is already taken
        if (library.isBookTaken(book)) {
            throw new IllegalArgumentException("The book " + book.getBarcode() 
            		+ " is already taken and not returned yet.");
        }
		
        // Check the date of taking the returned book
        for (BookRecord record : library.getBookRecords()) {
            if (record.getBook().equals(book) && takeDate.isBefore(record.getActualReturnDate())) {
            	throw new IllegalArgumentException("The book " + book.getBarcode() 
            		+ " is already taken and not returned yet.");
            }
        }
        
		subscription.addTakenBook(book);
		BookRecord bookRecord = new BookRecord(book, takeDate, plannedReturnDate);
        library.addBookRecord(bookRecord);
    }
	
	public void recordBookReturn(Library library,
			Subscription subscription, 
			Book book, 
			LocalDate actualReturnDate) {
		if (library.getAdministrator() != this) {
            throw new IllegalArgumentException("This administrator is not associated with "
            		+ "the provided library.");
        }
		BookRecord bookRecord = library.getBookRecords().stream()
				.filter(record -> record.getBook().equals(book)
                        && record.getActualReturnDate() == null)
        		.findFirst()
        		.orElse(null);
		
        if(bookRecord == null) {
        	throw new IllegalArgumentException("Book not found or already returned");
        }
        
        bookRecord.setActualReturnDate(actualReturnDate);
        subscription.removeReturnedBook(book);
    }
}
