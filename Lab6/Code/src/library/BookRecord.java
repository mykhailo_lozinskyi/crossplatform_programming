package library;

import java.time.LocalDate;
import java.io.Serializable;

public class BookRecord implements Serializable {
	private static final long serialVersionUID = 1L;
	private Book book;
    private LocalDate takeDate;
    private LocalDate plannedReturnDate;
    private LocalDate actualReturnDate;
    
	public BookRecord(Book book, LocalDate takeDate, LocalDate plannedReturnDate) {
		this.book = book;
		this.takeDate = takeDate;
		this.plannedReturnDate = plannedReturnDate;
	}

	public Book getBook() {
		return book;
	}

	public LocalDate getTakeDate() {
		return takeDate;
	}

	public LocalDate getPlannedReturnDate() {
		return plannedReturnDate;
	}

	public LocalDate getActualReturnDate() {
		return actualReturnDate;
	}

	public void setActualReturnDate(LocalDate actualReturnDate) {
		this.actualReturnDate = actualReturnDate;
	}
	
	public String getDescription() {
		StringBuilder bookRecordSB = new StringBuilder();
		bookRecordSB.append("Book barcode = " + this.book.getBarcode());
		bookRecordSB.append("; Take date = " + this.takeDate);
		bookRecordSB.append("; Planned return date = " + this.plannedReturnDate);
		if(this.actualReturnDate != null) {
			bookRecordSB.append("; Actual return date = " + this.actualReturnDate);
		}
		return bookRecordSB.toString();
	}
}
