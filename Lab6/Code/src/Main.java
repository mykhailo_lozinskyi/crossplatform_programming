import library.*;
import helpers.Reader;
import helpers.MailHelper;
import helpers.LibraryMailHelper;
import serialization.Serializer;

import java.util.List;
import java.util.Map;
import java.time.LocalDate;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.Optional;
import java.util.Comparator;
import java.io.IOException;

import java.lang.InterruptedException;


public class Main {
	public static void main(String[] args) {
		
		String booksFilePath = "src/resources/books.txt";
		String subscriptionsFilePath = "src/resources/subscriptions.txt";
		String bookRecordsFilePath = "src/resources/bookRecords.txt";
		
		Library library = getLibraryData(booksFilePath, subscriptionsFilePath, bookRecordsFilePath, "\t");
		if(library == null) {
			return;
		}
		
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		
		Scanner scanner = new Scanner(System.in);

		int operationChoice = 0;
		String choicesString = "\nSelect the desired operation: "
							   + "\n1 - Task 1"
							   + "\n2 - Task 2"
							   + "\n3 - Task 3"
							   + "\n4 - Task 4"
							   + "\n5 - Task 5"
							   + "\n6 - Task 6"
							   + "\n7 or any other number - Exit";
		while(true) {
			operationChoice = getUserChoice(scanner, choicesString);
			switch(operationChoice) {
				case 1: 
					handleTask1(library, scanner);
					break;
				case 2:
					handleTask2(library, scanner);
					break;
				case 3:
					handleTask3(library, scanner);
					break;
				case 4:
					handleTask4(library, scanner);
					break;
				case 5:
					handleTask5(library, scanner, executorService);
					break;
				case 6:
					handleTask6(library, scanner);
					break;
				default:
					scanner.close();
					Serializer.serializeLibrary(library, "src/resources/library.ser");
					
					// Wait for all asynchronous tasks to complete before exiting
				    executorService.shutdown();
				    try {
				    	if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
				    		executorService.shutdownNow();
					    } 
					} catch (InterruptedException e) {
						executorService.shutdownNow();
					}
				    
					System.out.println("\nThe program completed its execution successfully!");
					return;
			}
		}
	}
	
	private static Library getLibraryData(String booksFilePath, String subscriptionsFilePath, String bookRecordsFilePath, String splitter) {
		List<Book> booksList = null;
		try {
			booksList = Reader.readBooks(booksFilePath, splitter);
		} catch(IOException e) {
			System.out.println("The specified file \"" + booksFilePath + "\" could not be found.");
			System.out.println("\nThe library does not contain any books.".toUpperCase());
			return null;
		}
		
		Library library = new Library(booksList, new Administrator("admin1"));
		try {
			library.setSubscriptions(Reader.readSubscriptions(subscriptionsFilePath, splitter, library));
		} catch(IOException e) {
			System.out.println("The specified file \"" + subscriptionsFilePath + "\" could not be found.");
		}
		if(!(library.getSubscriptions() == null) && !(library.getSubscriptions().isEmpty())) {
			try {
				library.setBookRecords(Reader.readBookRecords(bookRecordsFilePath, splitter, library));
			} catch(IOException e) {
				System.out.println("The specified file \"" + bookRecordsFilePath + "\" could not be found.");
			}
		}
		return library;
	}
	
	private static int getUserChoice(Scanner scanner, String choiceString) {
		while (true) {
			System.out.println(choiceString);
			System.out.print("Your choice: ");
			try {
				return scanner.nextInt();
			} catch (InputMismatchException e) {
				scanner.nextLine();
				System.out.println("Invalid input. Please enter a number.\n".toUpperCase());
			}
		 }
	}

	private static void handleTask1(Library library, Scanner scanner) {
		String choiceString = "Select the sort order:"
							  + "\n1 - in ascending order;"
							  + "\n2 or any other number - in descending order";
		int orderChoice = getUserChoice(scanner, choiceString);
		boolean reverse = (orderChoice == 1) ? false: true;
		
		choiceString = "Choose how to display books:"
				+ "\n1 - display a complete list of books (all available copies of a certain book)"
				+ "\n2 or any other number - display only existing books, not all instances of a "
				+ "particular book";
		int fullChoice = getUserChoice(scanner, choiceString);
		boolean fullList = (fullChoice == 1) ? true: false;
		System.out.println("Task 1: Sort all books by year of publication.");
		
		var sortedList = library.getSortedBooksByYearOfPublication(reverse);
		
		if(!fullList) {
			int longestAuthorNameLength = 15;
			Optional<String> longestAuthorNameOptional = sortedList.stream()
			        .map(Book::getAuthor)
			        .max(Comparator.comparingInt(String::length));
			
			if(longestAuthorNameOptional.isPresent()) {
				longestAuthorNameLength = longestAuthorNameOptional.get().length();
			}
			System.out.println(String.format("Year\t%" + longestAuthorNameLength + "s\tTitle", "Author"));
			var booksList = sortedList.stream()
	                				  .collect(Collectors.toMap(
	                						  	book -> List.of(book.getAuthor(), book.getTitle(), book.getYearOfPublication()),
	                						  	book -> book,
	                						  	(existing, replacement) -> existing,
	                						  	LinkedHashMap::new
	                				  ))
	                				  .values()
	                				  .stream()
	                				  .collect(Collectors.toList());
			int innerlongestAuthorNameLength = longestAuthorNameLength;
			booksList.stream().forEach((book) -> System.out.println(
										String.format(book.getYearOfPublication() 
													  + "\t%" + innerlongestAuthorNameLength + "s\t" 
													  + book.getTitle(), book.getAuthor())));
			 
		} else {
			System.out.println(String.format("Year\t%12s\tTitle", "Barcode"));
			sortedList.forEach(book -> System.out.println(
					book.getYearOfPublication() + "\t" + book.getBarcode() + "\t\"" + book.getTitle() + "\""));
		}
		
	}
	
	private static void handleTask2(Library library, Scanner scanner) {
		String choiceString = "Enter the number of books taken (will be used as a condition for the mailing)";
		int numberOfTakenBooks = 0;
		while(true) {
			numberOfTakenBooks = getUserChoice(scanner, choiceString);
			if(numberOfTakenBooks >= 0) {
				break;
			}
			System.out.println("The number of taken books cannot be a negative number.\n".toUpperCase());
		}
		
		System.out.println("Task 2: Create a list of mailing addresses for readers who have taken more than "
				+ numberOfTakenBooks + " books.");
		
		var emailsList = library.getReaderEmails(numberOfTakenBooks);
		System.out.println("\nEmails:");
		emailsList.forEach(System.out::println);
	}
	
	private static void handleTask3(Library library, Scanner scanner) {
		List<String> authorsList = 
				library.getBooks().stream().map(book -> book.getAuthor()).distinct().toList();
		StringBuilder authorsChoiceSb = new StringBuilder();
		authorsChoiceSb.append("Choose an author (enter the corresponding number):");
		int authorIndex = 0;
		for(var author: authorsList) {
			authorIndex += 1;
			authorsChoiceSb.append("\n" + authorIndex + " - " + author);	
		}
		
		int selectedAuthorIndex = 0;
		while(true) {
			selectedAuthorIndex = getUserChoice(scanner, authorsChoiceSb.toString());
			if(selectedAuthorIndex > 0 && selectedAuthorIndex <= authorIndex) {
				break;
			}
			System.out.println("You have selected a non-existent author. ".toUpperCase()
					+ "Please enter a number from the list provided.".toUpperCase());
		}
		
		String authorToCheck = authorsList.get(selectedAuthorIndex - 1);
		var count = library.getNumberOfTakenAuthorBooks(authorToCheck);
		System.out.println(count + " users took " + authorToCheck + "'s books.");
	}
	
	private static void handleTask4(Library library, Scanner scanner) {
		int maxTakenBooks = library.getLargestNumberOfTakenBooks();
		if (maxTakenBooks != 0) {
		    
		    List<Subscription> readersWithMaxBooks = library.getReadersWithLargestNumberOfTakenBooks();

		    System.out.println("The largest number of books taken by a library reader is: " + maxTakenBooks);
		    System.out.println("Readers who have taken the maximum number of books:");
		    readersWithMaxBooks.forEach(reader -> System.out.println("- " + reader.getFirstName() + " " + reader.getLastName()));
		} else {
		    System.out.println("Readers did not take home a single book from the library.");
		}
	}
	
	private static CompletableFuture<Boolean> handleTask5(Library library, Scanner scanner, ExecutorService executorService) {
		String choiceString = "Enter the number of books taken (will be used as a condition for the mailing)";
		int numberOfBooks = 0;
		while(true) {
			numberOfBooks = getUserChoice(scanner, choiceString);
			if(numberOfBooks >= 0) {
				break;
			}
			System.out.println("The number of books cannot be a negative number.\n".toUpperCase());
		}
		
		CompletableFuture<Boolean> emailsFuture = null;
		Map<Integer, List<Subscription>> userGroups = library.getUserGroups(numberOfBooks);
		System.out.println("Group 1: Users with less than " + numberOfBooks + " books (inform about novelties)");
		var firstGroup = userGroups.getOrDefault(1, List.of());
		firstGroup.stream().forEach(user -> 
							System.out.println("- " + user.getFirstName() + " " + user.getLastName()));
		if (firstGroup.stream().anyMatch(subscription ->
        								 subscription.getEmail().equals(System.getenv("JavaLab6ReceiverEmail")))) {
			emailsFuture = 
			LibraryMailHelper.sendNoveltiesEmail(library, System.getenv("JavaLab6ReceiverEmail"), numberOfBooks, executorService);
		}
		
		
		System.out.println("\nGroup 2: Users with " + numberOfBooks + " or more books "
				+ "(inform about the timeliness of book return)");
		var secondGroup = userGroups.getOrDefault(2, List.of());
		secondGroup.forEach(user -> 
							System.out.println("- " + user.getFirstName() + " " + user.getLastName()));
		
		Optional<Subscription> subscriptionWithEmail = secondGroup.stream()
		        .filter(subscription -> subscription.getEmail().equals(System.getenv("JavaLab6ReceiverEmail")))
		        .findFirst();
		if (subscriptionWithEmail.isPresent()) {
		    Subscription foundSubscription = subscriptionWithEmail.get();
		    emailsFuture = LibraryMailHelper.sendReturnReminderEmail(library, foundSubscription, executorService);
		}
		
		return emailsFuture;
	}
	
	private static void handleTask6(Library library, Scanner scanner) {
		String currentDateStr = "";
		scanner.nextLine();
		while(true) {
			System.out.println("Enter a date to create a list of debtors."
					+ " The date must be entered in the format: yyyy-mm-dd (for example, 2023-11-12).");
			currentDateStr = scanner.nextLine().trim();
			if(currentDateStr.matches("\\d{4}-\\d{2}-\\d{2}")) {
				break;
			}
			System.out.println("Incorrect input format.".toUpperCase());
		}
		
		LocalDate currentDate =  LocalDate.parse(currentDateStr);
		
		var debtorsInfoList = library.getDebtorsInfo(currentDate);
		if(debtorsInfoList.isEmpty()) {
			System.out.println("There are no debtors on the specified date.");
		} else {
			debtorsInfoList.stream().forEach(System.out::println);
		}
		
	}
		
}
