package serialization;

import java.io.*;
import library.*;

public class Serializer {
	public static void serializeLibrary(Library library, String filePath) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath))) {
            oos.writeObject(library);
            System.out.println("\nLibrary serialized successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Library deserializeLibrary(String filePath) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath))) {
            Library library = (Library) ois.readObject();
            System.out.println("\nLibrary deserialized successfully.");
            return library;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
