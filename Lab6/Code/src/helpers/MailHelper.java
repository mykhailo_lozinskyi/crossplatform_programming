package helpers;

import java.io.Serializable;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailHelper implements Serializable {
	private String senderEmail;
	private String senderPassword;
	
	public MailHelper(String senderEmail, String senderPassword) {
		this.senderEmail = senderEmail;
		this.senderPassword = senderPassword;
	}
	
	public String getSenderEmail() {
		return senderEmail;
	}

	public boolean sendEmail(String receiverEmail, String emailSubject, String emailText) {
		try {
			Properties properties = new Properties();
			properties.setProperty("mail.smtp.host", "smtp.gmail.com");
			properties.setProperty("mail.smtp.port", "465");
			properties.setProperty("mail.smtp.auth", "true");
			properties.setProperty("mail.smtp.socketFactory.port", "465");
			properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			
			var senderEmail = this.getSenderEmail();
			var senderPasword = this.senderPassword;
			
			// Create a connection to gmail server
			Session session = Session.getInstance(properties, new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(senderEmail, senderPassword);
				}
			});
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(senderEmail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiverEmail));
			message.setSubject(emailSubject);
			message.setText(emailText);
			
			Transport.send(message);
			
			return true;
			
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
