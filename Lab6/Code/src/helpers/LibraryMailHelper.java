package helpers;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import library.Library;
import library.Subscription;

public class LibraryMailHelper {
	
	public static CompletableFuture<Boolean> sendNoveltiesEmail(Library library, String receiverEmail, int numberOfNovelties,
			ExecutorService executorService) {
		return CompletableFuture.supplyAsync(() -> {
			var novelties = library.getNovelties(numberOfNovelties);
			String emailSubject = "Library Novelties";
			StringBuilder emailTextSb = new StringBuilder();
			emailTextSb.append("Novelties of the library: \n");
			int noveltyIndex = 1;
			for(var novelty: novelties) {
				emailTextSb.append(noveltyIndex + ") " + novelty.getAuthor() + " - \"" + novelty.getTitle() + "\";\n");
				noveltyIndex += 1;
			}
			String emailText = emailTextSb.toString();
			emailText = emailText.replaceAll(";\\n$", ".").concat("\nIf you are interested in one of these books, please visit our library.");
			
			return library.getMailHelper().sendEmail(receiverEmail, emailSubject, emailText);
		}, executorService);
	}
	
	public static CompletableFuture<Boolean> sendReturnReminderEmail(Library library, Subscription subscription, ExecutorService executorService) {
		var booksToReturn = library.getBooksToReturn(subscription);
		
		if(booksToReturn.isEmpty()) {
			throw new IllegalArgumentException("Reader has no books to return");
		}
		
		return CompletableFuture.supplyAsync(() -> {
			String emailSubject = "Reminder about returning library books";
			StringBuilder emailTextSb = new StringBuilder();
			emailTextSb.append("Dear reader!\n");
			
			String bookStr = (booksToReturn.size() == 1) ? "book": "books";
			
			emailTextSb.append("You have taken the following " + bookStr + " from the \"SkillLibrary\":\n");

			int bookIndex = 1;
			for(var bookRecord: booksToReturn) {
				emailTextSb.append(bookIndex + ") " + bookRecord.getBook().getAuthor() + " - \"" 
						+ bookRecord.getBook().getTitle() + "\", Planned return date: " + bookRecord.getPlannedReturnDate() + ";\n");
				bookIndex += 1;
			}
			String emailText = emailTextSb.toString();
			emailText = emailText.replaceAll(";\\n$", ".").concat("\nPlease return the " + bookStr + " on time.");
			return library.getMailHelper().sendEmail(subscription.getEmail(), emailSubject, emailText);
		}, executorService);
	}
}
