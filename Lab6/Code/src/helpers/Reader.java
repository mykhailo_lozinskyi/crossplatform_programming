package helpers;

import library.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.time.LocalDate;

public class Reader {
	public static List<Book> readBooks(String filePath, String splitter) throws IOException {
		return Files.lines(Path.of(filePath))
                    .map(line -> {
                        String[] parts = line.split(splitter);
                        if (parts.length == 4) {
                            String barcode = parts[0];
                            String author = parts[1];
                            String title = parts[2];
                            int yearOfPublication = Integer.parseInt(parts[3]);

                            return new Book(barcode, author, title, yearOfPublication);
                        } else {
                            // Handle invalid lines
                            return null;
                        }
                    })
                    .filter(book -> book != null)
                    .collect(Collectors.toList());
    }
	
	public static List<Subscription> readSubscriptions(String filePath, String splitter, Library library) throws IOException {
		return Files.lines(Path.of(filePath))
                    .map(line -> {
                        String[] parts = line.split(splitter);
                        if (parts.length >= 4) {
                            String firstName = parts[0];
                            String lastName = parts[1];
                            String middleName = parts[2];
                            String email = parts[3];

                            Subscription subscription = new Subscription(firstName, lastName, middleName, email);

                            for (int i = 4; i < parts.length; i++) {
                                String barcode = parts[i];
                                Book book = library.getBookByBarcode(barcode);
                                if (book != null) {
                                    subscription.addTakenBook(book);
                                }
                            }

                            return subscription;
                        }
                        return null;
                    })
                    .filter(subscription -> subscription != null)
                    .collect(Collectors.toList());
    }
	
	public static List<BookRecord> readBookRecords(String filePath, String splitter, Library library) throws IOException {
	    return Files.lines(Path.of(filePath))
	            .map(line -> {
	                String[] parts = line.split(splitter);
	                if (parts.length >= 3) {
	                    String barcode = parts[0];
	                    Book book = library.getBookByBarcode(barcode);

	                    if (book != null) {
	                        LocalDate takeDate = LocalDate.parse(parts[1]);
	                        LocalDate plannedReturnDate = LocalDate.parse(parts[2]);

	                        BookRecord bookRecord = new BookRecord(book, takeDate, plannedReturnDate);

	                        if (parts.length >= 4 && !parts[3].isEmpty()) {
	                            // If actualReturnDate is present
	                            bookRecord.setActualReturnDate(LocalDate.parse(parts[3]));
	                        }

	                        return bookRecord;
	                    }
	                }
	                return null;
	            })
	            .filter(bookRecord -> bookRecord != null)
	            .collect(Collectors.toList());
	}
}
