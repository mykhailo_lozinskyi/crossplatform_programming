import gardentools.*;
import toolsmanager.GardenToolManager;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.InputMismatchException;

public class Main {
	
	public static void main(String[] args) {

	     List<GardenTool> gardenToolsList = new ArrayList<>();

	     // Create instances of Shovel
	     gardenToolsList.add(new Shovel("Shovel B", 19.99f, 2.5f, ToolType.LAND_CULTIVATION, 
	    		 Material.WOOD, 8.0f, Material.STEEL, 30.0f));
	     gardenToolsList.add(new Shovel("Shovel A", 24.99f, 2.8f, ToolType.LAND_CULTIVATION, 
	    		 Material.WOOD, 7.5f, Material.STEEL, 28.0f));

	     // Create instances of Pruner
	     gardenToolsList.add(new Pruner("Pruner A", 29.99f, 1.5f, ToolType.TREES_PROCESSING, 
	    		 Material.ALUMINUM, Material.STEEL, 8.0f, CuttingType.BYPASS, 0.75f));
	     gardenToolsList.add(new Pruner("Pruner B", 34.99f, 1.7f, ToolType.SITE_CLEANING, 
	    		 Material.ALUMINUM, Material.STEEL, 7.5f, CuttingType.ANVIL, 0.65f));
	     
	     // Create instances of WateringCan
	     gardenToolsList.add(new WateringCan("Watering Can A", 49.99f, 1.2f,
	    		 ToolType.WATERING_IRRIGATION, Material.PLASTIC, 5.0f, 0.5f, true));
	     gardenToolsList.add(new WateringCan("Watering Can B", 22.99f, 1.5f, 
	    		 ToolType.WATERING_IRRIGATION, Material.ALUMINUM, 4.0f, 0.4f, false));
	     
	     gardenToolsList.add(new Shovel("Shovel C", 20.99f, 2.25f, ToolType.LAND_CULTIVATION, 
	    		 Material.WOOD, 7.5f, Material.STEEL, 25.0f));
	     gardenToolsList.add(new Pruner("Pruner C", 18.99f, 1.75f, ToolType.SITE_CLEANING, 
	    		 Material.ALUMINUM, Material.STEEL, 8.5f, CuttingType.SCISSOR, 0.65f));
	     gardenToolsList.add(new WateringCan("Watering Can F", 14.99f, 1.25f, ToolType.TREES_PROCESSING, 
	    		 Material.PLASTIC, 5.0f, 0.5f, true));
	     gardenToolsList.add(new Shovel("Shovel K", 39.99f, 3.5f, ToolType.TREES_PROCESSING, 
	    		 Material.WOOD, 7.5f, Material.STEEL, 25.0f));
	     

	     GardenToolManager gardenToolManager = new GardenToolManager(gardenToolsList);
	     Scanner scanner = new Scanner(System.in);
	     int operationChoice = 0;
	     String choicesString = "Select the desired operation: "
	    	 		+ "\n1 - Search for garden tools"
	    	 		+ "\n2 - Sorting of garden tools"
	    	 		+ "\n3 or any other number - Exit";
	     while(true) {
	    	 operationChoice = getUserChoice(scanner, choicesString);
	    	 switch (operationChoice) { 
	    		 case 1:
	    			 handleSearchOperation(scanner, gardenToolManager);
	    			 break;
	    		 case 2:
	    			 handleSortOperation(scanner, gardenToolManager);
	    			 break;
	    	 	 default:
	    	 		scanner.close();
	    	 		System.out.println("The program completed its execution successfully!");
                    return;
	    	 }
	    }      
	}
	
	private static int getUserChoice(Scanner scanner, String choiceString) {
        while (true) {
        	System.out.println(choiceString);
            System.out.print("Your choice: ");
            try {
                return scanner.nextInt();
            } catch (InputMismatchException e) {
                scanner.nextLine(); // Consume the invalid input
                System.out.println("Invalid input. Please enter a number.\n".toUpperCase());
            }
        }
	}
	
	public static void displayResults(List<GardenTool> tools, String message) {
        System.out.println(message);
        for (GardenTool tool : tools) {
            System.out.println(tool.getDescription() + "\n");
        }
        System.out.println("=====================================");
    }
	
	private static void handleSearchOperation(Scanner scanner, GardenToolManager gardenToolManager) {
		int subOperationChoice = 0;
		boolean isValidInput = false;
		while(!isValidInput)
		{
			System.out.println("\nSelect a product category: "
					+ "\n1 - Land cultivation"
					+ "\n2 - Site cleaning"
					+ "\n3 - Sowing"
					+ "\n4 - Trees processing"
					+ "\n5 - Watering and irrigation");
			System.out.print("Your choice: ");
			try {
				subOperationChoice = scanner.nextInt();
				ToolType toolType = ToolType.fromValue(subOperationChoice);
				var resultList = gardenToolManager.searchByType(toolType);
				if(resultList.isEmpty()) {
					displayResults(resultList, 
						"\nUnfortunately, there is currently no tool for '" + toolType.name() 
						+ "' work.");
				} else {
					displayResults(resultList, 
						"\nAvailable tools for performing '" + toolType.name() + "' works: ");
				}
				isValidInput = true;
			} catch (InputMismatchException e) {
				scanner.nextLine();
	            System.out.println("Invalid input. Please enter a number.\n".toUpperCase());
			} catch (IllegalArgumentException e) {
				scanner.nextLine();
				System.out.println("Invalid input. Please enter a valid number (from 1 to 5).".toUpperCase());
			}
		}
	}
	
	private static void handleSortOperation(Scanner scanner, GardenToolManager gardenToolManager) {
		String subOperationChoicesString = "\nSelect the property you want to sort by: "
				+ "\n1 - Name"
				+ "\n2 - Price"
				+ "\n3 - Weight"
				+ "\n4 or any other number - Tool type";
		
		String sortOrderChoicesString = "\nSelect the sorting direction:"
				+ "\n1 - in ascending order"
				+ "\n2 or any other number - in descending order";
		int subOperationChoice = getUserChoice(scanner, subOperationChoicesString);
		int sortOrder = getUserChoice(scanner, sortOrderChoicesString);
		boolean reverse = (sortOrder != 1) ? true: false;
		String sortBy = "Name";
		switch(subOperationChoice) {
			case 1:
				gardenToolManager.sortByName(reverse);
				break;
			case 2:
				gardenToolManager.sortByPrice(reverse);
				sortBy = "Price";
				break;
			case 3:
				gardenToolManager.sortByWeight(reverse);
				sortBy = "Weight";
				break;
			default:
				gardenToolManager.sortByType(reverse);
				sortBy = "Tool type";
				break;
		}
		String order = (sortOrder != 1) ? "descending": "asceding";
		displayResults(gardenToolManager.getGardenTools(), 
				"\nSort results by " + sortBy + " in " + order + " order");
	}
}
