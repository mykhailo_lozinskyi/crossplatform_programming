package gardentools;

public class Pruner extends GardenTool{
	private Material bladeMaterial;
	private float bladeLength;
	private CuttingType cuttingType;
	private float cuttingCapacity;
	
	public Pruner(String name, float price, float weight, ToolType type, Material handleMaterial,
			Material bladeMaterial, float bladeLength, CuttingType cuttingType, float cuttingCapacity) {
		super(name, price, weight, type, handleMaterial);
		this.bladeMaterial = bladeMaterial;
		this.bladeLength = bladeLength;
		this.cuttingType = cuttingType;
		this.cuttingCapacity = cuttingCapacity;
	}

	public Material getBladeMaterial() {
		return bladeMaterial;
	}

	public void setBladeMaterial(Material bladeMaterial) {
		this.bladeMaterial = bladeMaterial;
	}

	public float getBladeLength() {
		return bladeLength;
	}

	public void setBladeLength(float bladeLength) {
		this.bladeLength = bladeLength;
	}

	public CuttingType getCuttingType() {
		return cuttingType;
	}

	public void setCuttingType(CuttingType cuttingType) {
		this.cuttingType = cuttingType;
	}

	public float getCuttingCapacity() {
		return cuttingCapacity;
	}

	public void setCuttingCapacity(float cuttingCapacity) {
		this.cuttingCapacity = cuttingCapacity;
	}

	@Override
	public String toString() {
		return "Pruner {" + 
				super.toString() +
				", bladeMaterial=" + bladeMaterial + 
				", bladeLength=" + bladeLength + 
				", cuttingType=" + cuttingType + 
				", cuttingCapacity=" + cuttingCapacity + 
				"}";
	}

	@Override
	public String getDescription() {
		return super.getDescription() + 
				"\nBlade material = " + bladeMaterial.name() +
				String.format("\nBlade length = %.2f", bladeLength) + " m" + 
				"\nCutting type = " + cuttingType.name() +
				String.format("\nCutting capacity = %.2f", cuttingCapacity) + " m";
	}	
}
