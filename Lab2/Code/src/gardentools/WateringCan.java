package gardentools;

public class WateringCan extends GardenTool{
	private float capacity;
	private float spoutDiameter;
	private boolean hasFlowControl;

	public WateringCan(String name, float price, float weight, ToolType type, Material handleMaterial, 
			float capacity, float spoutDiameter, boolean hasFlowControl) {
		super(name, price, weight, type, handleMaterial);
		this.capacity = capacity;
		this.spoutDiameter = spoutDiameter;
		this.hasFlowControl = hasFlowControl;
	}

	public float getCapacity() {
		return capacity;
	}

	public void setCapacity(float capacity) {
		this.capacity = capacity;
	}

	public float getSpoutDiameter() {
		return spoutDiameter;
	}

	public void setSpoutDiameter(float spoutDiameter) {
		this.spoutDiameter = spoutDiameter;
	}

	public boolean isHasFlowControl() {
		return hasFlowControl;
	}

	public void setHasFlowControl(boolean hasFlowControl) {
		this.hasFlowControl = hasFlowControl;
	}

	@Override
	public String toString() {
		return "WateringCan {" +
				super.toString() +
				", capacity=" + capacity + 
				", spoutDiameter=" + spoutDiameter + 
				", hasFlowControl=" + hasFlowControl +  
				"}";
	}

	@Override
	public String getDescription() {
		return super.getDescription() +
				String.format("\nCapacity = %.2f", capacity) + " m" +
				String.format("\nSpout diameter = %.2f", spoutDiameter) + " m" +
				(hasFlowControl ? "\nHas flow control": "\nDoesn't have flow control");
	}	
}
