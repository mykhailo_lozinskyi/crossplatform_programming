package gardentools;

public enum ToolType {
	LAND_CULTIVATION(1),
	SITE_CLEANING(2),
	SOWING(3),
	TREES_PROCESSING(4),
	WATERING_IRRIGATION(5);
	
	private final int value;

    ToolType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    
    // Map an integer value to the corresponding ToolType enum constant
    public static ToolType fromValue(int value) {
        for (ToolType toolType : ToolType.values()) {
            if (toolType.getValue() == value) {
                return toolType;
            }
        }
        throw new IllegalArgumentException("Invalid ToolType value: " + value);
    }
}
