package gardentools;

public class Shovel extends GardenTool{
	private float bladeWidth;
	private Material bladeMaterial;
	private float handleLength;
	
	public Shovel(String name, float price, float weight, ToolType type, Material handleMaterial, float bladeWidth,
			Material bladeMaterial, float handleLength) {
		super(name, price, weight, type, handleMaterial);
		this.bladeWidth = bladeWidth;
		this.bladeMaterial = bladeMaterial;
		this.handleLength = handleLength;
	}
	
	public float getBladeWidth() {
		return bladeWidth;
	}

	public void setBladeWidth(float bladeWidth) {
		this.bladeWidth = bladeWidth;
	}

	public Material getBladeMaterial() {
		return bladeMaterial;
	}

	public void setBladeMaterial(Material bladeMaterial) {
		this.bladeMaterial = bladeMaterial;
	}

	public float getHandleLength() {
		return handleLength;
	}

	public void setHandleLength(float handleLength) {
		this.handleLength = handleLength;
	}
	
	@Override
	public String toString() {
		return "Shovel {" +
				 super.toString() +
				 ", bladeWidth=" + bladeWidth + 
				 ", bladeMaterial=" + bladeMaterial + 
				 ", handleLength=" + handleLength +  
				 "}";
	}
	
	@Override
	public String getDescription() {
		return super.getDescription() +
				String.format("\nBlade width = %.2f", bladeWidth) + " m" +
				"\nBlade material = " + bladeMaterial.name() + 
				String.format("\nHandle length = %.2f", handleLength) + " m";
	}
	
}
