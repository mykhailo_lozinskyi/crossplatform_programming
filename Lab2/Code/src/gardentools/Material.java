package gardentools;

public enum Material {
	STEEL,
	ALUMINUM,
	PLASTIC,
	WOOD,
	FIBERGLASS
}
