package gardentools;

public abstract class GardenTool {
	protected String name;
	protected float price;
	protected float weight;
	protected ToolType type;
	protected Material handleMaterial;
	
	public GardenTool(String name, float price, float weight, ToolType type, Material handleMaterial)
	{
		this.name = name;
		this.price = price;
		this.weight = weight;
		this.type = type;
		this.handleMaterial = handleMaterial;
	}
	
	public String getName() {
		return name;
	}

	public float getPrice() {
		return price;
	}

	public float getWeight() {
		return weight;
	}

	public ToolType getType() {
		return type;
	}

	public Material getHandleMaterial() {
		return handleMaterial;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setPrice(float price) {
		this.price = price;
	}
	
	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	public void setType(ToolType type) {
		this.type = type;
	}
	
	public void setHandleMaterial(Material handleMaterial) {
		this.handleMaterial = handleMaterial;
	}
	
	@Override
	public String toString(){
		return "GardenTool{" + 
			   "name='" + name + "\'" +
			   ", price=" + price + 
			   ", weight=" + weight +
			   ", type=" + type.name() +
			   ", handleMaterial=" + handleMaterial.name() + 
			   "}";
	}
	
	public String getDescription() {
		return "Name = '" + name + "\'" +
			    String.format("\nPrice = %.2f", price) + " $" + 
			    String.format("\nWeight = %.2f", weight) + " kg" + 
			    "\nType = " + type.name() +
			    "\nHandle material = " + handleMaterial.name();
	}
}
