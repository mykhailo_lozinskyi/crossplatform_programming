package gardentools;

public enum CuttingType {
	BYPASS,
	ANVIL,
	RATCHET,
	SCISSOR,
	LOPPER
}
