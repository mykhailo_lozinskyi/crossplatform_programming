package toolsmanager;

import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;

import gardentools.GardenTool;
import gardentools.ToolType;

public class GardenToolManager {
	private List<GardenTool> gardenTools;

	public GardenToolManager(List<GardenTool> gardenTools) {
		this.gardenTools = gardenTools;
	}

	public List<GardenTool> getGardenTools() {
		return gardenTools;
	}

	public void setGardenTools(List<GardenTool> gardenTools) {
		this.gardenTools = gardenTools;
	}
	
    public List<GardenTool> searchByType(ToolType desiredToolType) {
    	List<GardenTool> matchingTools = new ArrayList<GardenTool>();
    	
        for (GardenTool tool : gardenTools) {
            if (tool.getType() == desiredToolType) {
                matchingTools.add(tool);
            }
        }
        return matchingTools;
    }
    
    // Overloaded searchByType function with a default value
    public List<GardenTool> searchByType() {
        return searchByType(ToolType.TREES_PROCESSING);
    }
    
	private static class GardenToolNameComparator implements Comparator<GardenTool> {
        @Override
        public int compare(GardenTool tool1, GardenTool tool2) {
            return tool1.getName().compareTo(tool2.getName());
        }
	}
	
	public void sortByName(boolean reverse) {
		Comparator<GardenTool> comparator = new GardenToolNameComparator();
		if(reverse){
			comparator = comparator.reversed();
		}
		Collections.sort(gardenTools, comparator);
	}
	
    private class GardenToolPriceComparator implements Comparator<GardenTool> {
        @Override
        public int compare(GardenTool tool1, GardenTool tool2) {
            // Compare by the 'price' attribute
            return Float.compare(tool1.getPrice(), tool2.getPrice());
        }
    }
    
    public void sortByPrice(boolean reverse) {
    	Comparator<GardenTool> comparator = new GardenToolPriceComparator();
    	if(reverse) {
    		comparator = comparator.reversed();
    	}
    	Collections.sort(gardenTools, comparator);
    }
    
    public void sortByWeight(boolean reverse) {
    	Comparator<GardenTool> comparator = new Comparator<GardenTool>() {
    		@Override
            public int compare(GardenTool tool1, GardenTool tool2) {
                // Compare by the 'price' attribute
                int result = Float.compare(tool1.getWeight(), tool2.getWeight());
                return reverse ? -result: result;
            }
    	};
    	Collections.sort(gardenTools, comparator);
    }
    
    public void sortByType(boolean reverse) {
    	if(reverse) {
    		gardenTools.sort((tool1, tool2) -> tool2.getType().compareTo(tool1.getType()));
    	} else {
    		gardenTools.sort((tool1, tool2) -> tool1.getType().compareTo(tool2.getType()));
    	}
    }	
}
