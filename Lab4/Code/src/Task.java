import java.time.*;
import java.time.temporal.*;

public class Task {

	public static void main(String[] args) {
		
        LocalDate currentDate = LocalDate.now();
        LocalDate firstMonday = currentDate.with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));

        Month currentMonth = currentDate.getMonth();
        while (firstMonday.getMonth() == currentMonth) {
            System.out.println(firstMonday);
            firstMonday = firstMonday.plusWeeks(1);
        }
	}

}