package trip;

import java.util.Objects;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TripPoint {
	private Coordinates location;
	private String name;
	private ZoneId timezone;
	private ZonedDateTime arrivalTime;
	private int index;
	
	public TripPoint(Coordinates location, String name, ZoneId timezone) {
		this.location = location;
		this.name = name;
		this.timezone = timezone;
	}

	public TripPoint(Coordinates location, String name, ZoneId timezone, ZonedDateTime arrivalTime) {
		this.location = location;
		this.name = name;
		this.timezone = timezone;
		this.arrivalTime = arrivalTime;
	}
	
	public TripPoint(String name, Coordinates location, ZoneId timezone) {
		this.name = name;
		this.location = location;
		this.timezone = timezone;
	}

	public Coordinates getLocation() {
		return location;
	}

	public void setLocation(Coordinates location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ZoneId getTimezone() {
		return timezone;
	}

	public void setTimezone(ZoneId timezone) {
		this.timezone = timezone;
	}

	public ZonedDateTime getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(ZonedDateTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public String toString() {
		return "TripPoint{"
				+ "index=" + index
				+ ",location=" + location.toString() + 
				",name=" + name + 
				",timezone=" + timezone + 
				",arrivalTime=" + arrivalTime + 
				"}";
	}
	
	public String getDescription(boolean addArrivalTime) {
		StringBuilder descriptionSb = new StringBuilder();
		descriptionSb.append("name = " + name 
							 + ", timezone = " + timezone
							 );
		if(addArrivalTime) {
			if(this.arrivalTime != null) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				descriptionSb.append(", arrivalTime = " + arrivalTime.format(formatter));
			}
			else
			{
				descriptionSb.append(", arrivalTime = " + arrivalTime);
			}
		}
		String description = descriptionSb.toString();		
		return description;					 
	}
	
	@Override 
	public int hashCode() {
		return Objects.hash(this.name, this.location, this.timezone, this.arrivalTime, this.index);
	}
	
	@Override
    public boolean equals(Object o) { 
        if (o == this) {
            return true;
        }
 
        if ((o == null) || (this.getClass() != o.getClass())) {
            return false;
        }
        
        TripPoint otherTripPoint = (TripPoint) o;
        return Objects.equals(this.location, otherTripPoint.getLocation())
        	   && Objects.equals(this.name, otherTripPoint.getName())
        	   && Objects.equals(this.timezone, otherTripPoint.getTimezone())
        	   && Objects.equals(this.arrivalTime, otherTripPoint.getArrivalTime())
        	   && this.index == otherTripPoint.getIndex();
    }
}
