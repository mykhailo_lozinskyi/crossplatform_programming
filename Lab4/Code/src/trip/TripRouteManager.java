package trip;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;

import helpers.DateTimeHelper;
import helpers.DijkstraHelper;
import helpers.DistanceHelper;
import graph.*;

public class TripRouteManager {
	private TripRoute tripRoute;
	private Graph tripRouteGraph;
	private List<TripPoint> tripPoints;
	private List<TripPoint> optimalRoute;
	
	public TripRouteManager(TripRoute tripRoute) {
		this.tripRoute = tripRoute;
	}

	public TripRoute getTripRoute() {
		return tripRoute;
	}

	public void setTripRoute(TripRoute tripRoute) {
		this.tripRoute = tripRoute;
	}
	
	public List<TripPoint> getTripPoints() {
		return tripPoints;
	}

	public Graph getRouteGraph() {
		return tripRouteGraph;
	}
	
	public List<TripPoint> getOptimalRoute() {
		return optimalRoute;
	}

	public void createGraph() {
		if(tripRoute.getStopPoints().isEmpty()) {
			return;
		}
		Graph graph = new Graph();
		
		// Add vertexes and indexes
		tripRoute.getStartPoint().setIndex(0);
		graph.addVertex(tripRoute.getStartPoint());
		
		int vertexIndex = 1;
		for(var stopPoint: tripRoute.getStopPoints()) {
			stopPoint.setIndex(vertexIndex);
			graph.addVertex(stopPoint);
			vertexIndex += 1;
		}
		
		tripRoute.getEndPoint().setIndex(vertexIndex);
        graph.addVertex(tripRoute.getEndPoint());
        
        // Add edges
        for(var outerStopPoint: tripRoute.getStopPoints()) {
        	graph.addEdge(tripRoute.getStartPoint(), outerStopPoint);
        	for(var innerStopPoint: tripRoute.getStopPoints()) {
        		if(!outerStopPoint.equals(innerStopPoint)) {
        			graph.addEdge(outerStopPoint, innerStopPoint);
        		}
        	}
        	graph.addEdge(outerStopPoint, tripRoute.getEndPoint());
        }
        graph.addEdge(tripRoute.getStartPoint(), tripRoute.getEndPoint());
        this.tripRouteGraph = graph;
        
        ArrayList<TripPoint> graphVertexes = new ArrayList<TripPoint>();
        graphVertexes.add(this.getTripRoute().getStartPoint());
        graphVertexes.addAll(this.getTripRoute().getStopPoints());
        graphVertexes.add(this.getTripRoute().getEndPoint());
        
        this.tripPoints = graphVertexes;
	}
	
	public void calculateRoute() {
		if (tripRoute.getStopPoints().isEmpty()) {
			double routeTime = getTimeForTwoPoints(tripRoute.getStartPoint(), tripRoute.getEndPoint());
			tripRoute.setRouteTime(routeTime);
			setPointArrivalTime(tripRoute.getStartPoint(), tripRoute.getEndPoint(), routeTime);
			
			List<TripPoint> optimalRoute = new ArrayList<TripPoint>();
			optimalRoute.add(tripRoute.getStartPoint());
			optimalRoute.add(tripRoute.getEndPoint());
			
			this.optimalRoute = optimalRoute;
		}
		else if (tripRoute.getStopPoints().size() == 1) {
			
			double pointsRouteTime = getTimeForTwoPoints(tripRoute.getStartPoint(), tripRoute.getStopPoints().get(0));
			double routeTime = pointsRouteTime;
			setPointArrivalTime(tripRoute.getStartPoint(),  tripRoute.getStopPoints().get(0), pointsRouteTime);
			
			pointsRouteTime = getTimeForTwoPoints(tripRoute.getStopPoints().get(0), tripRoute.getEndPoint());
			setPointArrivalTime(tripRoute.getStopPoints().get(0),  tripRoute.getEndPoint(), pointsRouteTime);
			
			routeTime += pointsRouteTime;
			
			List<TripPoint> optimalRoute = new ArrayList<TripPoint>();
			optimalRoute.add(tripRoute.getStartPoint());
			optimalRoute.add(tripRoute.getStopPoints().get(0));
			optimalRoute.add(tripRoute.getEndPoint());
			this.optimalRoute = optimalRoute;
			
			if((this.tripRoute.getTimeLimit() != 0.0) && (this.tripRoute.getTimeLimit() - routeTime < 1e-3)) {
				// Try route without stopPoint
				double routeTimeWithoutStopPoint = 0;
				routeTimeWithoutStopPoint = getTimeForTwoPoints(tripRoute.getStartPoint(), tripRoute.getEndPoint());
				
				if(routeTimeWithoutStopPoint < routeTime) {
					routeTime = routeTimeWithoutStopPoint;
					
					setPointArrivalTime(tripRoute.getStartPoint(),  tripRoute.getEndPoint(), routeTimeWithoutStopPoint);
					// Set arrival time of stop point to null
					this.tripRoute.getStopPoints().get(0).setArrivalTime(null);
					this.optimalRoute.remove(this.tripRoute.getStopPoints().get(0));
				}
			}
			tripRoute.setRouteTime(routeTime);
			
		} else {
			double[][] weightedMatrix = DijkstraHelper.getWeightedMatrix(this.getRouteGraph(), this.getTripPoints());
	        
	        double limitDistance = this.getTripRoute().getTimeLimit() * this.getTripRoute().getTransport().getSpeed();
	        if(limitDistance <= 1e-2) {
	        	System.out.println("Limit Distance = 0");
	        	limitDistance = Double.MAX_VALUE;
	        }
	        TripRouteResult optimalRouteResult = DijkstraHelper.findOptimalRoute(weightedMatrix, this.getTripRoute().getStartPoint(), 
	        		this.getTripRoute().getEndPoint(), this.getTripPoints());
	        
	        List<TripPoint> optimalRoute = optimalRouteResult.getRoute();
	        
	        int pointToRemoveEdgeIndex = optimalRoute.size() - 2;
	        TripPoint pointToRemoveEdge = optimalRoute.get(pointToRemoveEdgeIndex);
	        Graph tempGraph = new Graph(this.getRouteGraph());
	        tempGraph.removeEdge(pointToRemoveEdge, this.getTripRoute().getEndPoint());
	        
	        while(true) {
	        	 double[][] currentWeightedMatrix = DijkstraHelper.getWeightedMatrix(tempGraph, this.getTripPoints());
	        	 TripRouteResult currentOptimalRouteResult = DijkstraHelper.findOptimalRoute(currentWeightedMatrix, 
	        			 this.getTripRoute().getStartPoint(), this.getTripRoute().getEndPoint(),
	        			 this.getTripPoints());
	             
	             List<TripPoint> currentOptimalRoute = currentOptimalRouteResult.getRoute();
	             double currentOptimalDistance = currentOptimalRouteResult.getMinDistance();
	             
	             if((currentOptimalDistance <= limitDistance) && (currentOptimalRoute != null)) {
	            	 // Remove edge for the next iteration
	            	 pointToRemoveEdgeIndex = currentOptimalRoute.size() - 2;
	                 pointToRemoveEdge = currentOptimalRoute.get(pointToRemoveEdgeIndex);
	                 if(currentOptimalRoute.size() < 4) {
	                	 if(currentOptimalRoute.size() <= optimalRoute.size() && 
	                			 (tempGraph.getNumberOfEntries(this.getTripRoute().getEndPoint()) > 1)) { 
	                         tempGraph.removeEdge(pointToRemoveEdge, this.getTripRoute().getEndPoint());
	                     } else {
	                    	 tempGraph.removeEdge(this.getTripRoute().getStartPoint(), currentOptimalRoute.get(1));
	                     }
	                 } else {
	                	 // currentOptimalRoute.size() > = 4
	                	 if((tempGraph.getAdjacencyList().get(this.getTripRoute().getStartPoint()).size() == 1)
	                			 || ((tempGraph.getNumberOfEntries(this.getTripRoute().getEndPoint()) == 1)))
	                	 {
	                		 var edgeToRemove = DijkstraHelper.getEdgeToRemove(tempGraph, currentOptimalRoute);
	                		 if(edgeToRemove.isEmpty()) {
	                			 break;
	                		 }
	                		 tempGraph.removeEdge(edgeToRemove.get(0), edgeToRemove.get(1));
	                	 } else {
	                		 tempGraph.removeEdge(pointToRemoveEdge, this.getTripRoute().getEndPoint());
	                	 }
	                 }
	            	 optimalRoute = currentOptimalRoute;
	            	 
	             } else {
	            	 break;
	             }
	        }
	        this.optimalRoute = optimalRoute;
	        
	        // Walk through all points of the route, except the first (startPoint), in order to establish the time of arrival
	        double routeTime = 0;
	        int routeLength = optimalRoute.size();
	        for(int i=1; i<routeLength; i++) {
	        	TripPoint prevPoint = optimalRoute.get(i-1);
	        	TripPoint currPoint = optimalRoute.get(i);
	        	
	        	double currentRouteTime = getTimeForTwoPoints(prevPoint, currPoint);
	        	routeTime += currentRouteTime;
	        	
	        	setPointArrivalTime(prevPoint, currPoint, currentRouteTime);
	        }
	        this.getTripRoute().setRouteTime(routeTime);
		}
	}
	
	private double getTimeForTwoPoints(TripPoint startPoint, TripPoint endPoint) {
		double distance = DistanceHelper.calculateDistance(startPoint, endPoint);
		double routeTime = distance / tripRoute.getTransport().getSpeed();
		
		return routeTime;
	}
	
	private void setPointArrivalTime(TripPoint startPoint, TripPoint endPoint, double routeTime) {
		ZonedDateTime arrivalTime = DateTimeHelper.calculateArrivalTime(startPoint.getArrivalTime(), 
				routeTime, startPoint.getTimezone(), endPoint.getTimezone());
		endPoint.setArrivalTime(arrivalTime);
	}
	
	public String getOptimalRouteDescription() {
		StringBuilder optimalRouteSb = new StringBuilder();
		optimalRouteSb.append("Optimal Route: ");
		int longestNameLength = 0;
		for(var routePoint: optimalRoute) {
			if(!Objects.equals(routePoint, this.getTripRoute().getEndPoint())) {
				optimalRouteSb.append(routePoint.getName() + "->");
			} else {
				optimalRouteSb.append(routePoint.getName() + "\n");
			}
			if(routePoint.getName().length() > longestNameLength) {
				longestNameLength = routePoint.getName().length();
			}
		}
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		for(var routePoint: optimalRoute) {
			optimalRouteSb.append("Point: " + String.format("%-" + (longestNameLength + 2) + "s", routePoint.getName() + "; ")
					+ "Arrival Time: " + routePoint.getArrivalTime().format(formatter) + "; " 
					+ "Timezone: " + routePoint.getTimezone() + "\n");
		}
		
		optimalRouteSb.append("\nRoute start time: " + this.getTripRoute().getStartPoint().getArrivalTime().format(formatter));
		optimalRouteSb.append("\nRoute arrival time: " + this.getTripRoute().getEndPoint().getArrivalTime().format(formatter));
		
		optimalRouteSb.append("\nTime Limit: " + DateTimeHelper.formatTimeInHours(this.getTripRoute().getTimeLimit()));
		optimalRouteSb.append("\nRoute time: " + DateTimeHelper.formatTimeInHours(this.getTripRoute().getRouteTime()));
		
		optimalRouteSb.append("\n\nChosen Vehicle: " + this.getTripRoute().getTransport().toString());
		optimalRouteSb.append("\nVehicle Class: " + this.getTripRoute().getTransport().getClass().getSimpleName());
		
		return optimalRouteSb.toString();
	}
}
