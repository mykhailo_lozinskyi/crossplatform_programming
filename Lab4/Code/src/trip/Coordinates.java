package trip;

import java.util.Objects;

public class Coordinates {
	private double latitude;
	private double longitude;
	
	public Coordinates(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "Coordinates{"
				+ "latitude=" + latitude + 
				",longitude=" + longitude + 
				"}";
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.latitude, this.longitude);
	}
	
	@Override
    public boolean equals(Object o) {
    	 // If the object is compared with itself then return true  
        if (o == this) {
            return true;
        }
 
        if ((o == null) || (this.getClass() != o.getClass())) {
            return false;
        }
        
        Coordinates otherCoordinates = (Coordinates) o;
        return ((this.latitude == otherCoordinates.getLatitude()) && 
        		(this.longitude == otherCoordinates.getLongitude()));
    }
}
