package trip;

import java.util.List;
import java.time.format.DateTimeFormatter;

import vehicles.AbstractVehicle;
import helpers.DateTimeHelper;

public class TripRoute {
	private TripPoint startPoint;
	private TripPoint endPoint;
	private List<TripPoint> stopPoints;
	private AbstractVehicle transport;
	private float timeLimit;
	private double routeTime;
	
	public TripRoute(TripPoint startPoint, TripPoint endPoint, List<TripPoint> stopPoints, AbstractVehicle transport,
			float timeLimit) {
		super();
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		this.stopPoints = stopPoints;
		this.transport = transport;
		this.timeLimit = timeLimit;
	}

	public TripPoint getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(TripPoint startPoint) {
		this.startPoint = startPoint;
	}

	public TripPoint getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(TripPoint endPoint) {
		this.endPoint = endPoint;
	}

	public List<TripPoint> getStopPoints() {
		return stopPoints;
	}

	public void setStopPoints(List<TripPoint> stopPoints) {
		this.stopPoints = stopPoints;
	}

	public AbstractVehicle getTransport() {
		return transport;
	}

	public void setTransport(AbstractVehicle transport) {
		this.transport = transport;
	}

	public float getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(float timeLimit) {
		this.timeLimit = timeLimit;
	}

	public double getRouteTime() {
		return routeTime;
	}

	public void setRouteTime(double routeTime) {
		this.routeTime = routeTime;
	}

	public String getDescription(String firstString, boolean isInitialRoute) {
		StringBuilder descriptionSb = new StringBuilder();
		descriptionSb.append(firstString);
		descriptionSb.append("\nStart Point: " + startPoint.getDescription(!isInitialRoute));
		if(!stopPoints.isEmpty()) {
			for(TripPoint stopPoint: stopPoints) {
				if(stopPoint.getArrivalTime() != null) {
					descriptionSb.append("\nStop Point: " + stopPoint.getDescription(!isInitialRoute));
				}
			}
		}
		descriptionSb.append("\nEnd Point: " + endPoint.getDescription(!isInitialRoute));
		
		descriptionSb.append("\n\nChosen Vehicle: " + transport.toString());
		descriptionSb.append("\nVehicle Class: " + transport.getClass().getSimpleName());
		
		descriptionSb.append("\nTime Limit: " + DateTimeHelper.formatTimeInHours(timeLimit));
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		descriptionSb.append("\nRoute start time: " + startPoint.getArrivalTime().format(formatter));
		
		if(!isInitialRoute) {
			descriptionSb.append("\nRoute time: " + DateTimeHelper.formatTimeInHours(routeTime));
			descriptionSb.append("\nRoute arrival time: " + endPoint.getArrivalTime().format(formatter));
		}
		
		return descriptionSb.toString();
	}
}
