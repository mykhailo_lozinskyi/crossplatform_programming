package trip;

import java.util.List;

public class TripRouteResult {
	private List<TripPoint> route;
    private double minDistance;

    public TripRouteResult(List<TripPoint> route, double minDistance) {
        this.route = route;
        this.minDistance = minDistance;
    }

    public List<TripPoint> getRoute() {
        return route;
    }

    public double getMinDistance() {
        return minDistance;
    }
}
