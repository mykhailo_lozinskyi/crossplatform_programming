package vehicles;

public class Bus extends AbstractVehicle{
	
	public Bus(String model, float speed, float fuelConsumption) {
		super(model, speed, fuelConsumption);
	}
	
	@Override
    public String toString() {
        return "Bus{" +
        	   "model='" + model + '\'' +
               ",speed=" + speed +
               ",fuelConsumption=" + fuelConsumption +
               "}";
    }
}
