package vehicles;

public class Car extends AbstractVehicle{
	
	public Car(String model, float speed, float fuelConsumption) {
		super(model, speed, fuelConsumption);
	}
	
	@Override
    public String toString() {
        return "Car{" +
        	   "model='" + model + '\'' +
               ",speed=" + speed +
               ",fuelConsumption=" + fuelConsumption +
               "}";
    }
}
