package vehicles;

public class Train extends AbstractVehicle{
	
	public Train(String model, float speed, float fuelConsumption) {
		super(model, speed, fuelConsumption);
	}
	
	@Override
    public String toString() {
        return "Train{" +
        	   "model='" + model + '\'' +
               ",speed=" + speed +
               ",fuelConsumption=" + fuelConsumption +
               "}";
    }
}
