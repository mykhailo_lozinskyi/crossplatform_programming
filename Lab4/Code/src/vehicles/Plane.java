package vehicles;

public class Plane extends AbstractVehicle{
	
	public Plane(String model, float speed, float fuelConsumption) {
		super(model, speed, fuelConsumption);
	}
	
	@Override
    public String toString() {
        return "Plane{" +
        	   "model='" + model + '\'' +
               ",speed=" + speed +
               ",fuelConsumption=" + fuelConsumption +
               "}";
    }
}
