package vehicles;

public abstract class AbstractVehicle {
	protected String model;
	protected float speed;
	protected float fuelConsumption;
	
	public AbstractVehicle() {}
	public AbstractVehicle(String model, float speed, float fuelConsumption) {
		this.model = model;
		this.speed = speed;
		this.fuelConsumption = fuelConsumption;
	}
	
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public float getSpeed() {
		return speed;
	}
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	public float getFuelConsumption() {
		return fuelConsumption;
	}
	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}
	
	public String getDescription() {
		return "Model = " + model +
	           String.format("\nSpeed = .2f km/h", speed) +
	           String.format("\nFuel Consumption = .2f liters/100km", fuelConsumption);
	}
}
