package helpers;

import java.lang.Math;

import trip.*;

public class DistanceHelper {
	private static final double EARTH_RADIUS = 6371.0; // Earth's radius in kilometers
	
	/**
	 * Calculates the distance between two geographical points using the Haversine formula
	*/
	public static double calculateDistance(TripPoint point1, TripPoint point2) {
        Coordinates coordinates1 = point1.getLocation();
        Coordinates coordinates2 = point2.getLocation();

        double lat1 = Math.toRadians(coordinates1.getLatitude());
        double lon1 = Math.toRadians(coordinates1.getLongitude());
        double lat2 = Math.toRadians(coordinates2.getLatitude());
        double lon2 = Math.toRadians(coordinates2.getLongitude());

        double dLat = lat2 - lat1;
        double dLon = lon2 - lon1;

        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dLon / 2), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double distance = EARTH_RADIUS * c; // Distance in kilometers
        return distance;
    }
}
