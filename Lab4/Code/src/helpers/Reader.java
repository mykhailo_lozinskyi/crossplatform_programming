package helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import java.util.ArrayList;
import java.util.LinkedList;
import java.lang.reflect.Constructor;
import java.time.ZoneId;

import vehicles.AbstractVehicle;
import trip.*;


public class Reader {
	public static <T extends AbstractVehicle> ArrayList<T> ReadVehicles(String filename, String splitter, Class<T> vehicleClass){
		try {
			ArrayList<T> result = new ArrayList<>();
			File file = new File(filename);
			Scanner myReader = new Scanner(file);
		    while (myReader.hasNextLine()) {
		    	String data = myReader.nextLine();
		    	var vehicleAttributes = data.split(splitter);
		    	Constructor<T> constructor = vehicleClass.getConstructor(String.class, float.class, float.class);
		    	T vehicle = constructor.newInstance(vehicleAttributes[0], 
		    			Float.parseFloat(vehicleAttributes[1]), Float.parseFloat(vehicleAttributes[2]));
		    	result.add(vehicle);
		    }
		    myReader.close();
			return result;
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred while reading the file.");
		} catch (Exception e) {
            e.printStackTrace();
       }
	   return null;
	}
	
	public static LinkedList<TripPoint> ReadPoints(String filename, String splitter){
		try {
			LinkedList<TripPoint> result = new LinkedList<TripPoint>();
			File file = new File(filename);
			Scanner myReader = new Scanner(file);
		    while (myReader.hasNextLine()) {
		    	String data = myReader.nextLine();
		    	var pointAttributes = data.split(splitter);
		    	TripPoint point = new TripPoint(pointAttributes[0], 
		    			new Coordinates(Double.valueOf(pointAttributes[1]), Double.valueOf(pointAttributes[2])),
		    			ZoneId.of(pointAttributes[3]));
		    	result.add(point);
		    }
		    myReader.close();
	    	return result;
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred while reading the file.");
		}
		return null;
	}
	
	// An overloaded version of the ReadPoints() method with a splitter parameter and default (splitter=",")
	public static LinkedList<TripPoint> ReadPoints(String filename){
		return Reader.ReadPoints(filename, ",");
	}
}
