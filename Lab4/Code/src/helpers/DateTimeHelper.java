package helpers;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.Duration;

import java.util.Random;

public class DateTimeHelper {
	
	public static ZonedDateTime getNDaysDateTime(int numberOfDays, ZoneId zoneId) {
		LocalDateTime currentLocalDateTime = LocalDateTime.now();
		
	    ZonedDateTime currentZonedDateTime = currentLocalDateTime.atZone(zoneId);    
	    ZonedDateTime nDaysZonedDateTime = currentZonedDateTime.plus(numberOfDays, ChronoUnit.DAYS);
	        
	    return nDaysZonedDateTime;
	}
	
	public static ZonedDateTime getRandomDateTime() {
		LocalDateTime currentDateTime = LocalDateTime.now();
        Random random = new Random();

        LocalDateTime minDateTime = currentDateTime;
        LocalDateTime maxDateTime = currentDateTime.plus(3, ChronoUnit.DAYS);

        ZoneId zone = ZoneId.systemDefault();

        long minMillis = minDateTime.atZone(zone).toInstant().toEpochMilli();
        long maxMillis = maxDateTime.atZone(zone).toInstant().toEpochMilli();
        long randomMillis = minMillis + random.nextLong() % (maxMillis - minMillis);

        ZonedDateTime randomDateTime = ZonedDateTime.ofInstant(java.time.Instant.ofEpochMilli(randomMillis), zone);

        if (randomDateTime.toLocalDateTime().isBefore(currentDateTime)) {
            randomDateTime = ZonedDateTime.of(currentDateTime, zone).plus(1, ChronoUnit.DAYS);
        }
        return randomDateTime;
	}
	
	public static ZonedDateTime calculateArrivalTime(ZonedDateTime startDateTime, double routeTime, ZoneId startZone, ZoneId endZone) {
        // Calculate the duration of the trip in seconds
        long tripDurationInSeconds = (long) (routeTime * 3600);

        // Create a Duration object from the trip duration
        Duration tripDuration = Duration.ofSeconds(tripDurationInSeconds);

        // Perform time zone conversions
        ZonedDateTime startInStartZone = startDateTime.withZoneSameInstant(startZone);
        ZonedDateTime arrivalInEndZone = startInStartZone.plus(tripDuration);

        // Convert the arrival time back to the end time zone
        ZonedDateTime arrivalInEndZoneFinal = arrivalInEndZone.withZoneSameInstant(endZone);

        return arrivalInEndZoneFinal;
    }
	
	public static String formatTimeInHours(double routeTimeInHours) {
	    int hours = (int) routeTimeInHours;
	    int minutes = (int) ((routeTimeInHours - hours) * 60);
	    int seconds = (int) ((routeTimeInHours - hours - (minutes / 60.0)) * 3600);

	    return hours + " hours " + minutes + " minutes " + seconds + " seconds";
	}
}
