package helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import graph.Graph;
import trip.TripPoint;
import trip.TripRouteResult;

public class DijkstraHelper {
	
	public static double[][] getWeightedMatrix(Graph graph, List<TripPoint> tripPoints) {
		Map<TripPoint, List<TripPoint>> adjacencyList = graph.getAdjacencyList();
        int numVertices = adjacencyList.size();
        
        // Initialize the weighted matrix with default values (e.g., infinity)
        double[][] weightedMatrix = new double[numVertices][numVertices];
        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                weightedMatrix[i][j] = Double.POSITIVE_INFINITY;
            }
        }
        
        for(int i = 0; i < numVertices; i++) {
        	TripPoint startVertex = tripPoints.get(i);
        	for(int j = 0; j < numVertices; j++) {
        		
        		if(i == j) {
        			weightedMatrix[i][j] = 0;
        			continue;
        		}
        		TripPoint endVertex = tripPoints.get(j);
        		if(graph.hasEdge(startVertex, endVertex)) {
        			weightedMatrix[i][j] = DistanceHelper.calculateDistance(startVertex, endVertex);
        		}
        	}
        }
        return weightedMatrix;
	}
	
	public static void printMatrix(double[][] matrix, int rows, int cols) {
		for(int i = 0; i < rows; i++) {
			System.out.print("[");
			for(int j = 0; j < cols; j++) {
				if(j != (cols - 1)) {
					System.out.print(String.format("%6.3f; ", matrix[i][j]));
				}
				else {
					System.out.print(String.format("%6.3f", matrix[i][j]));
				}
			}
			System.out.print("],\n");
		}
	}
	
	static class QueueNode implements Comparable<QueueNode> {
	    private TripPoint vertex;  // Represents the vertex or trip point
	    private double distance;  // Represents the distance from the source to this vertex

	    public QueueNode(TripPoint vertex, double distance) {
	        this.vertex = vertex;
	        this.distance = distance;
	    }

	    public TripPoint getVertex() {
	        return vertex;
	    }

	    public double getDistance() {
	        return distance;
	    }

	    @Override
	    public int compareTo(QueueNode other) {
	        return Double.compare(this.distance, other.distance);
	    }
	}
	
	public static TripRouteResult findOptimalRoute(double[][] weightedMatrix, TripPoint startPoint, TripPoint endPoint,
			List<TripPoint> tripPoints) {
	    PriorityQueue<QueueNode> priorityQueue = new PriorityQueue<>();
	    Map<TripPoint, Double> distanceMap = new HashMap<>();
	    Map<TripPoint, TripPoint> parentMap = new HashMap<>();
	    List<TripPoint> minDistanceRoute = null;
	    double minDistance = Double.POSITIVE_INFINITY;
	    
	    priorityQueue.add(new QueueNode(startPoint, 0.0));
	    
	    while (!priorityQueue.isEmpty()) {
	        QueueNode node = priorityQueue.poll();
	        TripPoint current = node.getVertex();
	        double currentDistance = node.getDistance();
	        int currentIdx = current.getIndex();

	        // Process neighbors
	        for (int neighborIdx = 0; neighborIdx < weightedMatrix[currentIdx].length; neighborIdx++) {
	            if (neighborIdx != currentIdx) {
	                double edgeWeight = weightedMatrix[currentIdx][neighborIdx];
	                double totalDistance = currentDistance + edgeWeight;

	                if (totalDistance < minDistance) {
	                    TripPoint neighbor = getTripPointByIndex(neighborIdx, tripPoints);
	                    if (totalDistance < distanceMap.getOrDefault(neighbor, Double.POSITIVE_INFINITY)) {
	                        distanceMap.put(neighbor, totalDistance);
	                        parentMap.put(neighbor, current);
	                        priorityQueue.add(new QueueNode(neighbor, totalDistance));
	                        
	                        if (neighbor.equals(endPoint) && totalDistance < minDistance) {
	                            minDistance = totalDistance;
	                            minDistanceRoute = buildOptimalRoute(parentMap, endPoint);
	                        }
	                    }
	                }
	            }
	        }
	    }

	    if (minDistanceRoute != null) {
	    	return new TripRouteResult(minDistanceRoute, minDistance);
	    } else {
	        // No route within the limit, return a route with minDistance
	    	return new TripRouteResult(buildOptimalRoute(parentMap, endPoint), minDistance);
	    }
	}

	
	private static List<TripPoint> buildOptimalRoute(Map<TripPoint, TripPoint> parentMap, TripPoint endPoint) {
	    List<TripPoint> route = new ArrayList<>();
	    TripPoint current = endPoint;

	    while (current != null) {
	        route.add(current);
	        current = parentMap.get(current);
	    }

	    Collections.reverse(route); // Reverse the route to start from startPoint to endPoint
	    return route;
	}
	
	public static TripPoint getTripPointByIndex(int index, List<TripPoint> tripPoints) {
        if (index >= 0 && index < tripPoints.size()) {
            return tripPoints.get(index);
        } else {
            throw new IndexOutOfBoundsException("Invalid index: " + index);
        }
    }
	
	public static ArrayList<TripPoint> getEdgeToRemove(Graph graph, List<TripPoint> route){
		ArrayList<TripPoint> edgeToRemove = new ArrayList<TripPoint>(); // 0 - edgeStartPoint, 1 - edgeEndPoint
		
		int routeSize = route.size();
		var adjacencyList = graph.getAdjacencyList();
		
		for(int i = 1; i < routeSize - 1; i++) {
			TripPoint currentPoint = route.get(i);
			if(adjacencyList.get(currentPoint).size() > 1) { // the current node contains links to other nodes
				edgeToRemove.add(currentPoint);
				edgeToRemove.add(route.get(i+1));
			}
		}
		edgeToRemove.trimToSize();
		return edgeToRemove;
	}
}
