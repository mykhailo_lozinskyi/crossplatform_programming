package graph;

import java.util.*;
import java.time.ZoneId;

import trip.*;

public class Graph {
	private Map<TripPoint, List<TripPoint>> adjacencyList;

    public Graph() {
        adjacencyList = new HashMap<>();
    }
    
    // Copy constructor
    public Graph(Graph other) {
        adjacencyList = new HashMap<>();

        // Copy vertices (TripPoint objects)
        for (TripPoint vertex : other.adjacencyList.keySet()) {
            this.addVertex(vertex);
        }

        // Copy edges
        for (Map.Entry<TripPoint, List<TripPoint>> entry : other.adjacencyList.entrySet()) {
            TripPoint source = entry.getKey();
            List<TripPoint> neighbors = entry.getValue();

            for (TripPoint destination : neighbors) {
                this.addEdge(source, destination);
            }
        }
    }
    
    public void addVertex(TripPoint vertex) {
        adjacencyList.put(vertex, new LinkedList<>());
    }

    public void addEdge(TripPoint source, TripPoint destination) {
        adjacencyList.get(source).add(destination);
    }
    
    public void removeEdge(TripPoint source, TripPoint destination) {
        List<TripPoint> sourceNeighbors = adjacencyList.get(source);
        if (sourceNeighbors != null) {
            sourceNeighbors.remove(destination);
        }
    }
    
    public List<TripPoint> getNeighbors(TripPoint vertex) {
        return adjacencyList.get(vertex);
    }

    public Map<TripPoint, List<TripPoint>> getAdjacencyList() {
        return adjacencyList;
    }
    
    public String getPointNeighborsNames(TripPoint tripPoint) {
        List<TripPoint> neighborsOfPoint = this.getNeighbors(tripPoint);
        StringBuilder neighborsSb = new StringBuilder();
        
        for(var neighbor: neighborsOfPoint) {
        	neighborsSb.append(neighbor.getName() + " ");
        }
        return neighborsSb.toString();
    }
    
    public String getGraphDescription() {
    	StringBuilder graphSb = new StringBuilder();
    	
    	for(var entry: adjacencyList.entrySet()) {
    		graphSb.append(entry.getKey().getName() + ": ");
    		graphSb.append(this.getPointNeighborsNames(entry.getKey()) + "\n");
    	}
    	return graphSb.toString();
    }
    
    public boolean hasEdge(TripPoint startPoint, TripPoint endPoint) {
    	if(this.adjacencyList.get(startPoint).contains(endPoint)) {
    		return true;
    	}
    	return false;
    }
    
    public int getNumberOfEntries(TripPoint tripPoint) {
    	int counter = 0;
    	for(var tripPointKey: adjacencyList.keySet()) {
    		if(tripPointKey == tripPoint) {
    			continue;
    		}
    		if(adjacencyList.get(tripPointKey).contains(tripPoint)) {
    			counter += 1;
    		}
    	}
    	return counter;
    }
    
    public static void main(String[] args) {
        Graph graph = new Graph();

        // Create TripPoint instances
        TripPoint startPoint = new TripPoint("Kyiv", new Coordinates(50.45466,30.52380), ZoneId.of("Europe/Kiev"));
        TripPoint stopPointA = new TripPoint("Warsaw", new Coordinates(52.22977,21.01178), ZoneId.of("Europe/Warsaw"));
        TripPoint stopPointB = new TripPoint("Berlin", new Coordinates(52.52437,13.41053), ZoneId.of("Europe/Berlin"));
        TripPoint endPoint = new TripPoint("London", new Coordinates(51.50853,-0.12574), ZoneId.of("Europe/London"));

        // Add vertices
        graph.addVertex(startPoint);
        graph.addVertex(stopPointA);
        graph.addVertex(stopPointB);
        graph.addVertex(endPoint);

        // Add edges
        graph.addEdge(startPoint, stopPointA);
        graph.addEdge(startPoint, stopPointB);

        graph.addEdge(stopPointA, stopPointB);
        graph.addEdge(stopPointB, stopPointA);
        
        graph.addEdge(stopPointA, endPoint);
        graph.addEdge(stopPointB, endPoint);
        
        System.out.println("The number of inputs to the startPoint is:" + graph.getNumberOfEntries(startPoint));
        System.out.println("The number of inputs to the stopPointA is:" + graph.getNumberOfEntries(stopPointA));
        System.out.println("The number of inputs to the stopPointB is:" + graph.getNumberOfEntries(stopPointB));
        System.out.println("The number of inputs to the endPoint is:" + graph.getNumberOfEntries(endPoint));
        
    }
}
