import java.util.ArrayList;
import java.util.LinkedList;

import java.util.Scanner;
import java.util.InputMismatchException;

import vehicles.*;
import helpers.*;
import trip.*;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		// Getting initial route
		TripRoute tripRoute = getInitialTripRoute(scanner);
		
		TripRouteManager routeManager = new TripRouteManager(tripRoute);
		if(routeManager.getTripRoute().getStopPoints().size()>1) {
			routeManager.createGraph();
		}
		routeManager.calculateRoute();
		
		if(((routeManager.getTripRoute().getTimeLimit() - routeManager.getTripRoute().getRouteTime()) < 1e-3) 
				&& (routeManager.getTripRoute().getTimeLimit() > 1e-3)) {
			System.out.println("\n/*****************************************************************/");
			System.out.println("\nThere is no route that meets the specified time limit.".toUpperCase());
			System.out.println("You can try to use other types or models of transport.");
			System.out.println("\nBelow is the optimal route found by the program.");
		}
		
		System.out.println("\n/*****************************************************************/");
        System.out.println(routeManager.getOptimalRouteDescription());
        
		scanner.close();
		System.out.println("\nThe program completed its execution successfully!");
	}
	
	private static int getUserChoice(Scanner scanner, String choiceString) {
        while (true) {
        	System.out.println(choiceString);
            System.out.print("Your choice: ");
            try {
                return scanner.nextInt();
            } catch (InputMismatchException e) {
                scanner.nextLine();
                System.out.println("Invalid input. Please enter a number.\n".toUpperCase());
            }
        }
	}
	
	private static TripPoint getRoutePoint(LinkedList<TripPoint> tripPoints, Scanner scanner, String choicesSting) {
		TripPoint tripPoint = null;
		StringBuilder tripChoicesSb = new StringBuilder();
		tripChoicesSb.append("\nAvailable Trip Points:");
        for (int i = 0; i < tripPoints.size(); i++) {
        	tripChoicesSb.append("\n" + (i + 1) + ". " + tripPoints.get(i).getName());
        }
        tripChoicesSb.append(choicesSting);
        String tripChoices = tripChoicesSb.toString();
        while(true) {
        	int startPointIndex = getUserChoice(scanner, tripChoices) - 1;
    		if((startPointIndex >= 0) && (startPointIndex < tripPoints.size())) {
    			tripPoint = tripPoints.get(startPointIndex);
    			tripPoints.remove(startPointIndex);
    			break;
    		}
    		else {
    			System.out.println("You entered a number that does not match any points. Please try again.\n".toUpperCase());
    		}
        }
        return tripPoint;
	}
	
	private static LinkedList<TripPoint> getRoutePoints(LinkedList<TripPoint> tripPoints, Scanner scanner){
		LinkedList<TripPoint> selectedPoints = new LinkedList<>(); // 0 - startPoint, 1 - endPoint, 2...n - stopPoints
		
		var startPoint = getRoutePoint(tripPoints, scanner, "\nSelect the start point of the route: ");
		selectedPoints.add(startPoint);
		var endPoint = getRoutePoint(tripPoints, scanner, "\nSelect the end point of the route: ");
		selectedPoints.add(endPoint);
		String hasStopsChoice = "\nDo you want to add stop points?"
				+ "\n1 - yes"
				+ "\n2 or any other number - no";
		
		int hasStops = getUserChoice(scanner, hasStopsChoice);
		if(hasStops == 1) {
			while(true) {
				var stopPoint = getRoutePoint(tripPoints, scanner, "\nSelect the stop point of the route: ");
				selectedPoints.add(stopPoint);
				if(tripPoints.size() == 0) {
					System.out.println("\nYou have selected all possible stop points.");
					break;
				}
				String continueStopsChoice = "\nDo you want to add another stop point?"
						+ "\n1 - yes"
						+ "\n2 or any other number - no";
				
				int continueStopPoints = getUserChoice(scanner, continueStopsChoice);
				if(continueStopPoints != 1) {
					break;
				}
			}
		}
		return selectedPoints;
	}
	
	private static AbstractVehicle getTransport(ArrayList<String> vehicleFilenames,
												ArrayList<Class<? extends AbstractVehicle>> vehicleClasses,
												Scanner scanner,
												String choicesString,
												int numberOfTransportTypes)
	{
		int transportTypeIndex = 0;
		while(true) {
			transportTypeIndex = getUserChoice(scanner, choicesString) - 1;
			if((transportTypeIndex >= 0) && (transportTypeIndex < numberOfTransportTypes)) {
    			break;
    		}
    		else {
    			System.out.println("You entered a number that does not match any points. Please try again.\n".toUpperCase());
    		}
		}
		
		ArrayList<? extends AbstractVehicle> vehiclesList = Reader.ReadVehicles(vehicleFilenames.get(transportTypeIndex), ",", 
				vehicleClasses.get(transportTypeIndex));
		StringBuilder modelChoicesSb = new StringBuilder();
		modelChoicesSb.append("\nChoose a specific model of transport:");
		for(int i=0; i<vehiclesList.size(); i++) {
			modelChoicesSb.append("\n" + (i + 1) + " - " + vehiclesList.get(i).toString());
		}
		String modelChoicesString = modelChoicesSb.toString();
		int numberOfVehicles = vehiclesList.size();
		int transportIndex = 0;
		while(true) {
			transportIndex = getUserChoice(scanner, modelChoicesString) - 1;
			if((transportIndex >= 0) && (transportIndex < numberOfVehicles)) {
    			break;
    		}
    		else {
    			System.out.println("You entered a number that does not match any points. Please try again.\n".toUpperCase());
    		}
		}
		
		AbstractVehicle transport = vehiclesList.get(transportIndex);
		return transport;
	}
	
	private static float getTimeLimit(Scanner scanner) {
		String timeLimitChoiceStr = "\nDo you want to enter time restrictions on the route?"
				+"\n1 - yes"
				+"\n2 or any other number - no";
		int hasTimeLimit = getUserChoice(scanner, timeLimitChoiceStr);
		float timeLimit = 0;
		if(hasTimeLimit == 1) {
			while(true) {
				System.out.println("\nEnter a time limit (in hours): ");
				try {
					timeLimit = scanner.nextFloat();
					if(timeLimit> 1e-3) {
						break;
					}
					else
					{
						System.out.println("Invalid value entered. Time cannot be negative. Please try again.".toUpperCase());
					}
		        } catch (InputMismatchException e) {
		        	scanner.nextLine();
		            System.out.println("Invalid input. Please enter the correct fractional number".toUpperCase()
		            		+ "(the whole part is separated from the fractional part with the sign ',' (comma)).".toUpperCase()); 
		        }
			}
		}
		return timeLimit;
	}
	
	private static TripRoute getInitialTripRoute(Scanner scanner) {
		// Selection of route points
		LinkedList<TripPoint> tripPoints = Reader.ReadPoints("src/resources/cities.txt");
		var selectedPoints = getRoutePoints(tripPoints, scanner);	// 0 - startPoint, 1 - endPoint, 2...n - stopPoints
		TripPoint startPoint = selectedPoints.remove(0);
		TripPoint endPoint = selectedPoints.remove(0);
				
		// Selection of transport
		ArrayList<String> vehicleFilenames = new ArrayList<String>() {{
			add("src/resources/Vehicles/planes.txt");
			add("src/resources/Vehicles/cars.txt");	
			add("src/resources/Vehicles/trains.txt");
			add("src/resources/Vehicles/buses.txt");
		}};
				
		ArrayList<Class<? extends AbstractVehicle>> vehicleClasses = new ArrayList<Class<? extends AbstractVehicle>>() {{
			add(Plane.class);
			add(Car.class);
			add(Train.class);
			add(Bus.class);
		}};
				
		String transportChoicesString = "\nSelect the type of transport: "
			    	 + "\n1 - Plane"
			    	 + "\n2 - Car"
			    	 + "\n3 - Train"
			    	 + "\n4 - Bus";
				
		var transport = getTransport(vehicleFilenames, vehicleClasses, scanner, transportChoicesString, vehicleClasses.size());
				
		// Specifying a time limit
		float timeLimit = getTimeLimit(scanner);
				
		// Obtaining a route start date and time
		var routeStartDateTime = DateTimeHelper.getNDaysDateTime(1, startPoint.getTimezone());
				
		// Create an instance of the TripRoute class
		startPoint.setArrivalTime(routeStartDateTime);
				
		TripRoute tripRoute = new TripRoute(startPoint, endPoint, selectedPoints, transport, timeLimit);
		return tripRoute;
	}
}