package tests;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;

import java.util.Set;
import java.util.HashSet;

import carsmanager.Car;

public class TestLists {
	public static void printListForLoop(List<Car> cars) {
		for(Car car: cars) {
			System.out.println(car.toString());
		}
	}
	
	public static void printListIterator(List<Car> cars, String sentence) {
		System.out.println(sentence);
		Iterator<Car> iterator = cars.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next().toString());
		}
	}
	
	public static void printListIterator(List<Car> cars) {
		Iterator<Car> iterator = cars.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next().toString());
		}
	}
	
	public static void searchExample() {
		List<Car> cars = new ArrayList<Car>();
		cars.add(new Car("Toyota Camry", 25000.0, 180));
		cars.add(new Car("Honda Accord", 27000.0, 200));
		cars.add(new Car("Ford Mustang", 35000.0, 200));
		
		Car carToSearch = new Car("Honda Accord", 27000.0, 200);
		var result = cars.indexOf(carToSearch);
		System.out.println("result = " + result);
	}
	
	public static void subListExample() {
		// All actions performed on the subarray are also performed on the original array
		List<Car> cars = new ArrayList<Car>();
		cars.add(new Car("Toyota Camry", 25000.0, 180));
		cars.add(new Car("Honda Accord", 27000.0, 200));
		cars.add(new Car("Ford Mustang", 35000.0, 200));
		
		System.out.println("cars List before changing model");
		printListIterator(cars);
		
		List<Car> carsSublist = cars.subList(0, 1);
		System.out.println("\ncarsSublist before changing model ");
		printListIterator(carsSublist);
		
		carsSublist.get(0).setModel("New Model");
		System.out.println("\ncarsSublist after changing model");
		printListIterator(carsSublist);
		
		System.out.println("\ncars List after changing model");
		printListIterator(cars);
	}
	
	public static void InsertLinkedList() {
		LinkedList<Car> cars = new LinkedList<Car>();
		cars.add(new Car("Toyota Camry", 25000.0, 180));
		cars.add(new Car("Honda Accord", 27000.0, 200));
		cars.add(new Car("Ford Mustang", 35000.0, 200));
		cars.add(2, new Car("Hyundai Sonata", 23000.0, 180));
		
		printListIterator((List<Car>)cars, "LinkedList Insert");
	}
	
	public static void ListToSetTest() {
		// Requires overriding the equals() and hashCode() methods in custom class
		List<Car> cars = new ArrayList<Car>();
		cars.add(new Car("Toyota Camry", 25000.0, 180));
		cars.add(new Car("Honda Accord", 27000.0, 200));
		cars.add(new Car("Ford Mustang", 35000.0, 200));
		cars.add(new Car("Ford Mustang", 35000.0, 200));
		System.out.println("cars List");
		printListIterator(cars);
		
		Set<Car> carsSet = new HashSet<Car>(cars);
		System.out.println("\ncars Set");
		printSetIterator(carsSet);
	}
	
	public static void printSetIterator(Set<Car> carsSet) {
		Iterator<Car> iterator = carsSet.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
