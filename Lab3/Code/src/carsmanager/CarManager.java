package carsmanager;

import java.util.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CarManager {
	private List<Car> cars = new ArrayList<Car>();
	
	public CarManager() {
		
	}
	
	public CarManager(List<Car> cars) {
		this.cars = cars;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	
	public void readCars(String filename, String splitter) {
		try {
		    File file = new File(filename);
		    Scanner myReader = new Scanner(file);
		    while (myReader.hasNextLine()) {
		      String data = myReader.nextLine();
		      var carAttributes = data.split(splitter);
		      Car car = new Car(carAttributes[0], 
		        Double.valueOf(carAttributes[1]), 
		        Integer.valueOf(carAttributes[2]));
		      cars.add(car);
		    }
		    myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred while reading the file.");
		}
	}
	
	public void readCars(String filename) {
		this.readCars(filename, " ");
	}
	
	public Map<Integer, List<Car>> getCarsBySpeed(){
		Map<Integer, List<Car>> speedMap = new HashMap<>();
		for (Car car : cars) {
            int speed = car.getMaximumSpeed();
            speedMap.computeIfAbsent(speed, k -> new ArrayList<>()).add(car);
        }
		return speedMap;
	}
	
	public Map<Integer, List<Car>> getCarsBySpeedWithoutModels(Set<String> modelsToRemove) {
		Map<Integer, List<Car>> speedMap = getCarsBySpeed();
		if(speedMap.isEmpty()) {
			return speedMap;
		}
		for(Map.Entry<Integer, List<Car>> entry: speedMap.entrySet()) {
			List<Car> carsWithSpeed = entry.getValue();
			carsWithSpeed.removeIf(car -> modelsToRemove.contains(car.getModel()));
		}
		return speedMap;
	}
	
	public void ExtendsAndSort(List<Car> secondList, boolean reverse) {
		cars.addAll(secondList);
		Collections.sort(cars);
		if(reverse) {
			Collections.reverse(cars);
		}
	}
	
	public void ExtendsAndSort(List<Car> secondList) {
		// reverse = true
		cars.addAll(secondList);
		Collections.sort(cars);
		Collections.reverse(cars);

	}
	
	public String getCarsDescription() {
		if(cars.isEmpty()) {
			return "The car list is empty.";
		}
		String resultString = "";
		for(Car car: cars) {
			resultString += car.toString() + "\n";
		}
		return resultString;
	}
}
