package carsmanager;

public class Car implements Comparable<Car> {
	protected String model;
    protected double cost;
    protected int maximumSpeed;

    public Car(String model, double cost, int maximumSpeed) {
        this.model = model;
        this.cost = cost;
        this.maximumSpeed = maximumSpeed;
    }

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public int getMaximumSpeed() {
		return maximumSpeed;
	}

	public void setMaximumSpeed(int maximumSpeed) {
		this.maximumSpeed = maximumSpeed;
	}
    
	@Override
	public int compareTo(Car otherCar) {
		return this.getModel().compareTo(otherCar.getModel());
	}
	
    @Override
    public String toString() {
    	return "Car{" + 
    			"model='" + model + '\'' +
                ", cost=" + cost +
                ", maximumSpeed=" + maximumSpeed +
				"}";
    }
    
    public String getDescription() {
    	return "Model = '" + model + '\'' +
    			String.format("\nCost = %.2f", cost) + 
    			"\nMaximum Speed = " + maximumSpeed;
    }
}
