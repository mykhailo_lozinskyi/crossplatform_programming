import java.util.ArrayList;

public class Task {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> numbersArr = new ArrayList<Integer>();
		for(int i = 1; i <= 10; i++) {
			numbersArr.add(i);
		}
		
		numbersArr.removeIf(i -> (i%2 == 0));
		
		for(var num: numbersArr) {
			System.out.println(num);
		}
	}

}
