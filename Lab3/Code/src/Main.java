import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import carsmanager.Car;
import carsmanager.CarManager;

public class Main {

	public static void main(String[] args) {
		
		String filename = "src/resources/cars.txt";
		var carManager = new CarManager();
	    carManager.readCars(filename, ",");
	    
	    Scanner scanner = new Scanner(System.in);
	    
	    String secondFilename = "src/resources/cars2.txt";
	    
	    int operationChoice = 0;
	    String choicesString = "Select the desired operation: "
	    	 		+ "\n1 - Task 1"
	    	 		+ "\n2 - Task 2"
	    	 		+ "\n3 - Task 3"
	    	 		+ "\n4 or any other number - Exit";
	    while(true) {
	    	operationChoice = getUserChoice(scanner, choicesString);
	    	switch(operationChoice) {
	    		case 1:
	    			handleTask1(carManager, scanner);
	    			break;
	    		case 2:
	    			handleTask2(carManager, scanner);
	    			break;
	    		case 3:
	    			handleTask3(carManager, secondFilename, scanner);
	    			break;
	    		default:
	    			scanner.close();
	    		    System.out.println("\nThe program completed its execution successfully!");
	    		    return;
	    	}
	    }
	}
	
	private static int getUserChoice(Scanner scanner, String choiceString) {
        while (true) {
        	System.out.println(choiceString);
            System.out.print("Your choice: ");
            try {
                return scanner.nextInt();
            } catch (InputMismatchException e) {
                scanner.nextLine();
                System.out.println("Invalid input. Please enter a number.\n".toUpperCase());
            }
        }
	}
	
	private static void handleTask1(CarManager carManager, Scanner scanner) {
		var speedMap = carManager.getCarsBySpeed();
	    while(true) {
	    	try {
	    		System.out.println("Enter the number of cars at maximum speed to display for each "
			    		+ "speed indicator: ");
			    int numberOfCars = scanner.nextInt();
			    if(!speedMap.isEmpty()) {
			    	
			    	List<Map.Entry<Integer, List<Car>>> entryList = new ArrayList<>(speedMap.entrySet());
			    	entryList.sort(Map.Entry.comparingByKey(Comparator.reverseOrder()));
			    	//entryList.sort(Collections.reverseOrder(Map.Entry.comparingByKey()));
			    	
			    	for(Map.Entry<Integer, List<Car>> entry: entryList) {
				    	List<Car> carsWithSpeed = entry.getValue();
				    	
				    	System.out.println("\nCars with maximum speed " + entry.getKey() + ":");
				    	int count = 0;
				    	for(Car car: carsWithSpeed) {
				    		System.out.println(car.toString());
			                count += 1;
			                if (count >= numberOfCars) {
			                    break;
			                }
				    	}
				    }
			    } else {
			    	System.out.println("The car list is empty.");
			    }
			    break;
	    	} catch(InputMismatchException e) {
	    		System.out.println("Invalid input. Please enter a number.\n".toUpperCase());
		    	scanner.nextLine();
	    	}
	    }
	}
	
	private static void handleTask2(CarManager carManager, Scanner scanner) {
		Set<String> modelsToRemove = new HashSet<String>();
	    System.out.println("Enter car models to remove (type 'q' or 'exit' to finish):\n");
        String model;
        scanner.nextLine();
        while (true) {
        	System.out.println("Enter car model: ");
            model = scanner.nextLine().trim();
            if (model.equalsIgnoreCase("q") || model.equalsIgnoreCase("exit")) {
                break;
            }
            modelsToRemove.add(model);
        }
	    var speedMap = carManager.getCarsBySpeedWithoutModels(modelsToRemove);
	    printMap(speedMap, "\nCars with maximum speed ");
	}
	
	private static void handleTask3(CarManager carManager, String secondFilename, Scanner scanner) {
	    List<Car> secondList = readFile(secondFilename, ",");
	    carManager.ExtendsAndSort(secondList);
	    System.out.println("Extended cars list:\n" + carManager.getCarsDescription());
	    
	    while(true) {
	    	try {
		    	System.out.println("Enter the minimum and maximum prices of cars (separated by a space): ");
		        double minPrice = scanner.nextDouble();
		        double maxPrice = scanner.nextDouble();
		        if(maxPrice <= minPrice) {
		        	System.out.println("Invalid input. The maximum price must be greater than the minimum price.".toUpperCase());
		        	continue;
		        }
		        long carsInPriceRange = carManager.getCars().stream()
		                .filter(car -> car.getCost() >= minPrice && car.getCost() <= maxPrice)
		                .count();
			    
			    System.out.println("The number of cars whose prices are in the range [" + minPrice + "; " + maxPrice 
			    		+ "] is " + carsInPriceRange + ".");
			    
			    break;
		    } catch (InputMismatchException e) {
		    	System.out.println("Invalid input format. "
		    			+ "Please enter valid numeric values separated by a space." 
		    			+ "\nThe fractional part of each decimal is separated from the whole part with the symbol ',' "
		    			+ "(comma).".toUpperCase());
		    	scanner.nextLine();
		    }    
	    }
	}
	
	private static void printMap(Map<Integer, List<Car>> map, int numberOfCars, String msg) {
		if(!map.isEmpty()) {
	    	for(Map.Entry<Integer, List<Car>> entry: map.entrySet()) {
		    	List<Car> carsFromMap = entry.getValue();
		    	
		    	System.out.println(msg + entry.getKey() + ":");
		    	int count = 0;
		    	for(Car car: carsFromMap) {
		    		System.out.println(car.toString());
	                count += 1;
	                if (count >= numberOfCars) {
	                    break;
	                }
		    	}
		    }
	    } else {
	    	System.out.println("The map is empty.");
	    }
	}
	
	private static void printMap(Map<Integer, List<Car>> map, String msg) {	// numberOfCars = 0 
		if(!map.isEmpty()) {
	    	for(Map.Entry<Integer, List<Car>> entry: map.entrySet()) {
		    	List<Car> carsFromMap = entry.getValue();
		    	
		    	System.out.println(msg + entry.getKey() + ":");
		    	for(Car car: carsFromMap) {
		    		System.out.println(car.toString());
		    	}
		    }
	    } else {
	    	System.out.println("The map is empty.");
	    }
	}
	
	private static List<Car> readFile(String filename, String splitter) {
		try {
			List<Car> carsList = new ArrayList<Car>();
		    File file = new File(filename);
		    Scanner myReader = new Scanner(file);
		    while (myReader.hasNextLine()) {
		      String data = myReader.nextLine();
		      var carAttributes = data.split(splitter);
		      Car car = new Car(carAttributes[0], 
		        Double.valueOf(carAttributes[1]), 
		        Integer.valueOf(carAttributes[2]));
		      carsList.add(car);
		    }
		    myReader.close();
		    return carsList;
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred while reading the file.");
		}
		return null;
	}
}


