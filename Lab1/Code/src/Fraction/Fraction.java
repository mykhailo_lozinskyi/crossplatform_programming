package Fraction;

public class Fraction implements IFraction{
	private long numerator;
	private long denominator;
	
	public Fraction(long num, long denom) {
		this.numerator = num;
		this.denominator = denom;
		simplify();
	}
	
	// Methods
	private long gcd(long a, long b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }
	
	private void simplify() {
        long gcd = gcd(numerator, denominator);
        numerator /= gcd;
        denominator /= gcd;
    }
	
	@Override
    public Fraction add(IFraction other) {
        long newNumerator = this.numerator * ((Fraction) other).denominator
                + ((Fraction) other).numerator * this.denominator;
        long newDenominator = this.denominator * ((Fraction) other).denominator;
        return new Fraction(newNumerator, newDenominator);
    }
	
	@Override
    public String toString() {
        return numerator + "/" + denominator;
    }
}
