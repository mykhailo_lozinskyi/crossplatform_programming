package Fraction;
import java.math.BigInteger;

public class BigIntegerFraction implements IFraction{
	
	private BigInteger numerator;
	private BigInteger denominator;
	
	public BigIntegerFraction(BigInteger num, BigInteger denom) {
		this.numerator = num;
		this.denominator = denom;
		simplify();
	}
	
	public BigIntegerFraction(long num, long denom) {
		this.numerator = BigInteger.valueOf(num);
        this.denominator = BigInteger.valueOf(denom);
        simplify();
	}

	// Methods
	private void simplify() {
        BigInteger gcd = numerator.gcd(denominator);
        this.numerator = numerator.divide(gcd);
        this.denominator = denominator.divide(gcd);
    }
	
	@Override
	public IFraction add(IFraction other) {
		BigInteger newNumerator = this.numerator.multiply(((BigIntegerFraction) other).denominator)
                .add(((BigIntegerFraction) other).numerator.multiply(this.denominator));
        BigInteger newDenominator = this.denominator.multiply(((BigIntegerFraction) other).denominator);
        return new BigIntegerFraction(newNumerator, newDenominator);
	}
	
	@Override
	public String toString() {
		return numerator + "/" + denominator;
	}
}
