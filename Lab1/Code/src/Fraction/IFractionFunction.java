package Fraction;

public interface IFractionFunction {
	long apply(long n);
}
