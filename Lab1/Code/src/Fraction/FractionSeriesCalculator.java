package Fraction;
import java.lang.reflect.Constructor;
import java.lang.Math;
import java.util.Scanner;

public class FractionSeriesCalculator {
	
	private IFractionFunction numerator_function;
	private IFractionFunction denominator_function;
	
	public FractionSeriesCalculator(IFractionFunction numerator_function, 
			IFractionFunction denominator_function) {
		this.numerator_function = numerator_function;
		this.denominator_function = denominator_function;
	}
	
	public IFraction calculateSum(int n) {
        IFraction sum = null;
        
        if (n <= 15) {
            sum = new Fraction(this.numerator_function.apply(1), 
            		this.denominator_function.apply(1));
            for (int i = 2; i <= n; i++) {
                sum = sum.add(new Fraction(this.numerator_function.apply(i), 
                		this.denominator_function.apply(i)));
            }
        } else {
            sum = new BigIntegerFraction(this.numerator_function.apply(1), 
            		this.denominator_function.apply(1));
            for (int i = 2; i <= n; i++) {
                sum = sum.add(new BigIntegerFraction(this.numerator_function.apply(i), 
                		this.denominator_function.apply(i)));
            }
        }

        return sum;
    }
	
	public IFraction calculateSum(int n, Class<? extends IFraction> fractionClass) {
	    IFraction sum = null;

	    try {
	        Constructor<? extends IFraction> constructor = fractionClass.getConstructor(long.class, long.class);
	        sum = constructor.newInstance(this.numerator_function.apply(1), this.denominator_function.apply(1));

	        for (int i = 2; i <= n; i++) {
	            IFraction temp = constructor.newInstance(this.numerator_function.apply(i), this.denominator_function.apply(i));
	            sum = sum.add(temp);
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    return sum;
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the number of elements of the series: ");
		int n = scanner.nextInt();
		scanner.close();
		
		// Example: Calculate the sum of 2^n/3^n series
		FractionSeriesCalculator calculator1 = new FractionSeriesCalculator(
            i -> (long) Math.pow(2, i),
            i -> (long) Math.pow(3, i)
        );
        /*IFraction sum = calculator1.calculateSum(n);
		System.out.println("Sum for n = " + n + ": " + sum.toString());
		System.out.println("Type of sum object: " + sum.getClass().getName());*/
		
		// Second approach
		IFraction sum = null;

		if (n <= 15) {
            sum = calculator1.calculateSum(n, Fraction.class);
        } else {
            sum = calculator1.calculateSum(n, BigIntegerFraction.class);
        }

		System.out.println("\nSum for n = " + n + ": " + sum.toString());
		System.out.println("Type of sum object: " + sum.getClass().getName());
	}
	
	
	
}
