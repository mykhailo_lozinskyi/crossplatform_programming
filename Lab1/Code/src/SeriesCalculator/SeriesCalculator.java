package SeriesCalculator;
import java.math.BigInteger;
import java.util.Scanner;

public class SeriesCalculator {
	public static long gcd(long a, long b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }
	
	public static long[] simplify(long[] fraction) {
        long gcd = gcd(fraction[0], fraction[1]);
        return new long[] {fraction[0] /= gcd, fraction[1] /= gcd};
    }
	
	public static long[] addFractions(long[] fraction1, long[] fraction2) {
		long newNumerator = fraction1[0] * fraction2[1] + fraction2[0] * fraction1[1];
        long newDenominator = fraction1[1] * fraction2[1];
        return new long[] {newNumerator, newDenominator};
	}
	
	public static long[] calculateSum(int n) {
		System.out.println("Type long");
		// 1/n
		long[] fraction = new long[] {1, 1};
		long[] newFraction = new long[] {1, 1};
		for(int i = 2; i<=n; i++) {
			newFraction[1] = i;
			fraction = addFractions(fraction, newFraction);
		}
		return fraction;
	}
	
	// BigInteger version
	public static BigInteger gcd(BigInteger a, BigInteger b) {
        if (b.equals(BigInteger.ZERO)) {
            return a;
        }
        return gcd(b, a.mod(b));
    }
	
	public static BigInteger[] simplify(BigInteger[] fraction) {
        BigInteger gcd = gcd(fraction[0], fraction[1]);
        return new BigInteger[] {fraction[0].divide(gcd), fraction[1].divide(gcd)};
    }
	
	public static BigInteger[] addFractions(BigInteger[] fraction1, BigInteger[] fraction2) {
		BigInteger newNumerator = fraction1[0].multiply(fraction2[1]).add(fraction2[0].multiply(fraction1[1]));
        BigInteger newDenominator = fraction1[1].multiply(fraction2[1]);
        return new BigInteger[] {newNumerator, newDenominator};
	}
	
	public static BigInteger[] calculateSumBigInt(int n) {
		System.out.println("Type BigInt");
		// 1/n
		BigInteger[] fraction = new BigInteger[] {BigInteger.valueOf(1), BigInteger.valueOf(1)};
		BigInteger[] newFraction = new BigInteger[] {BigInteger.valueOf(1), BigInteger.valueOf(1)};
		for(int i = 2; i<=n; i++) {
			newFraction[1] = BigInteger.valueOf(i);
			fraction = addFractions(fraction, newFraction);
		}
		return fraction;
	}
	
	
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the number of elements of the series: ");
		int n = scanner.nextInt();
		
		// Sum(1/n)
		if(n<=15)
		{
			long[] sum_fraction = simplify(calculateSum(n));
			System.out.println("\nSum = " + sum_fraction[0] + "/" + sum_fraction[1]);
		}
		else
		{
			BigInteger[] bigint_sum_fraction = simplify(calculateSumBigInt(n));
			System.out.println("\nSum = " + bigint_sum_fraction[0] + "/" + bigint_sum_fraction[1]);	
		}
		scanner.close();
	}
}
